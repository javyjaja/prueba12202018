﻿document.getElementById("telABA").addEventListener("click", funtelABA);
document.getElementById("lnkLanzarChat").addEventListener("click", funlnkLanzarChat);
document.getElementById("mailABA").addEventListener("click", funmailABA);
document.getElementById("btnServiciosEnLinea").addEventListener("click", funserviciosEnLinea);
document.getElementById("homeIndex").addEventListener("click", funhomeIndex);
document.getElementById("nuestrosSeguros").addEventListener("click", funnuestrosSeguros);
document.getElementById("autoSeguro").addEventListener("click", funautoSeguro);
document.getElementById("autoCotizador").addEventListener("click", funautoCotizador);
document.getElementById("hogarSeguro").addEventListener("click", funhogarSeguro);
document.getElementById("casaCotizador").addEventListener("click", funcasaCotizador);
document.getElementById("pymeSeguro").addEventListener("click", funpymeSeguro);
document.getElementById("otrosSeguros").addEventListener("click", funotrosSeguros);
document.getElementById("quienesSomos").addEventListener("click", funquienesSomos);
document.getElementById("seguimientoSiniestro").addEventListener("click", funseguimientoSiniestro);
document.getElementById("pagoPoliza").addEventListener("click", funpagoPoliza);
document.getElementById("consultaPoliza").addEventListener("click", funconsultaPoliza);
document.getElementById("pagoDeducible").addEventListener("click", funpagoDeducible);
document.getElementById("consultaFactura").addEventListener("click", funconsultaFactura);
document.getElementById("seccionMapa").addEventListener("click", funseccionMapa);
document.getElementById("reportaSiniestro").addEventListener("click", funreportaSiniestro);
document.getElementById("btnCloseCuadro").addEventListener("click", funbtnCloseCuadro);

function funtelABA() {
    gtag('event', 'llamada', { 'event_category': 'home', 'event_label': '' });
}
function funlnkLanzarChat() {
    gtag('event', 'chat', { 'event_category': 'home', 'event_label': '' });
}
function funmailABA() {
    gtag('event', 'mail', { 'event_category': 'home', 'event_label': '' });
}
function funserviciosEnLinea() {
    $('.login-right-panel').show();
    gtag('event', 'servicios_en_linea', { 'event_category': 'home', 'event_label': '' });
}
function funhomeIndex() {
    gtag('event', 'logo', { 'event_category': 'home', 'event_label': '' });
}
function funnuestrosSeguros() {
    gtag('event', 'nuestros_seguros', { 'event_category': 'home', 'event_label': '' });
}
function funautoSeguro() {
    gtag('event', 'auto_seguro', { 'event_category': 'nuestros_seguros', 'event_label': 'ver_mas' });
}
function funautoCotizador() {
    gtag('event', 'auto_seguro', { 'event_category': 'nuestros_seguros', 'event_label': 'cotizar' });
}
function funhogarSeguro() {
    gtag('event', 'hogar_seguro', { 'event_category': 'nuestros_seguros', 'event_label': 'ver_mas' });
}
function funcasaCotizador() {
    gtag('event', 'hogar_seguro', { 'event_category': 'nuestros_seguros', 'event_label': 'cotizar' });
}
function funpymeSeguro() {
    gtag('event', 'pyme_segura', { 'event_category': 'nuestros_seguros', 'event_label': 'ver_mas' });
}
function funotrosSeguros() {
    gtag('event', 'otros_seguros', { 'event_category': 'nuestros_seguros', 'event_label': 'ver_mas' });
}
function funquienesSomos() {
    gtag('event', 'quienes_somos', { 'event_category': 'home', 'event_label': '' });
}
function funseguimientoSiniestro() {
    gtag('event', 'servicio_express', { 'event_category': 'home', 'event_label': 'seguimiento_a_siniestro' });
}
function funpagoPoliza() {
    gtag('event', 'servicio_express', { 'event_category': 'home', 'event_label': 'pago_de_poliza' });
}
function funconsultaPoliza() {
    gtag('event', 'servicio_express', { 'event_category': 'home', 'event_label': 'consulta_de_poliza' });
}
function funpagoDeducible() {
    gtag('event', 'servicio_express', { 'event_category': 'home', 'event_label': 'pago_de_deducibles' });
}
function funconsultaFactura() {
    gtag('event', 'servicio_express', { 'event_category': 'home', 'event_label': 'consulta_de_factura' });
}
function funseccionMapa() {
    gtag('event', 'servicio_express', { 'event_category': 'home', 'event_label': 'localizador_de_oficinas' });
}
function funreportaSiniestro() {
    gtag('event', 'servicio_express', { 'event_category': 'home', 'event_label': 'reportar_siniestro' });
}
function funbtnCloseCuadro() {

    $('.login-right-panel').slideUp();
}