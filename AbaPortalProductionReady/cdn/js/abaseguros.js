﻿$(function () {
    if (window.location.pathname.indexOf("/AutoCotizador") > -1) {
        $("#scrollBtn").hide();
    }

    // Floating Menu
    $(".nuestros-seguros-btn ,.dropdown.seguros").click(function () {

        if ($(window).width() > 767) {
            if ($(".nuestros-seguros-menu:visible").length > 0) {
                //muestra, ocultar
                
                if ($(".login-right-panel").is(':visible')) {
                    $(".login-right-panel").find(".sidebar-nav-fixed").animate({
                        top: $(".login-right-panel").find(".sidebar-nav-fixed").position().top - $(".nuestros-seguros-menu").height()
                    }, 200, function () { $(".nuestros-seguros-menu").hide(); });
                } else { $(".nuestros-seguros-menu").hide(); }
                //            $(this).parent().css('background-color', 'transparent');
            } else {
                //oculto, mostrar
                $(".nuestros-seguros-menu").show();
                if ($(".login-right-panel").is(':visible')) {
                    $(".login-right-panel").find(".sidebar-nav-fixed").animate({
                        top: $(".login-right-panel").find(".sidebar-nav-fixed").position().top + $(".nuestros-seguros-menu").height()
                    }, 500, function () {  });
                } else { $(".nuestros-seguros-menu").show(); }
                //            $(this).parent().css('background-color', 'black');
            }
        }
    });
    //Ocultar mega menu con click en cualquier parte excepto boton
    $(document).mouseup(function (e) {
        var container = $(".nuestros-seguros-menu");
        var btnDrop = $(".nuestros-seguros-btn ,.dropdown.seguros");
        // if the target of the click isn't the container nor a descendant of the container
        if ($(".nuestros-seguros-menu:visible").length > 0 && !container.is(e.target) && container.has(e.target).length === 0 && 
            !btnDrop.is(e.target) && btnDrop.has(e.target).length ===0) {
            
            if ($(".login-right-panel").is(':visible'))
            {
                $(".login-right-panel").find(".sidebar-nav-fixed").animate({
                    top: $(".login-right-panel").find(".sidebar-nav-fixed").position().top - $(".nuestros-seguros-menu").height()
                }, 200,function(){container.hide();});
            } else { container.hide(); }
        }
    });

    function goToByScroll(id) {
        // Remove "link" from the ID
        id = id.replace("link", "");

        // Scroll
        $('html,body').animate({
            scrollTop: $("#" + id).offset().top - 100
        },
            'slow');
    }

    $("#scrollBtn").click(function () {
        idToGo = $(this).attr("href").replace("#", "");
        goToByScroll(idToGo);
    });
    $(".scrollBtn").click(function () {
        idToGo = $(this).attr("href").replace("#", "");
        goToByScroll(idToGo);
    });
});