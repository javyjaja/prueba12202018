﻿document.getElementById("imprimePago").addEventListener("click", funimprimePago);
document.getElementById("enviaPago").addEventListener("click", funenviaPago);

function funimprimePago() {
    gtag('event', 'confirmacion_de_compra', { 'event_category': 'hogar_clon', 'event_label': 'imprime_pago' });
}
function funenviaPago() {
    gtag('event', 'confirmacion_de_compra', { 'event_category': 'hogar_clon', 'event_label': 'envia_pago' });
}