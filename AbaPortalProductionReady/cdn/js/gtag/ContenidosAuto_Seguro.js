﻿document.getElementById("arefAqui").addEventListener("click", funarefAqui);
document.getElementById("arefAqui2").addEventListener("click", funarefAqui2);
document.getElementById("arefEncuentra").addEventListener("click", funarefEncuentra);
document.getElementById("btnCotiza").addEventListener("click", funbtnCotiza);

function funarefAqui() {

    gtag('event', 'cotiza_en_linea', {'event_category': 'autoseguro','event_label': 'aqui'});
}
function funarefAqui2() {

    gtag('event', 'cotiza_en_linea', {'event_category': 'autoseguro','event_label': 'aqui'});
}
function funarefEncuentra() {

    gtag('event', 'banner', {'event_category': 'autoseguro','event_label': 'encuentra_un_agente'});
}
function funbtnCotiza() {

    gtag('event', 'banner', {'event_category': 'autoseguro','event_label': 'cotizar_ahora'});
}