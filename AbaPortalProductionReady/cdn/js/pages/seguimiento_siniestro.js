﻿$('#payFirstStep').click(function (e) {
    $('#Lightbox').show();
    $('#Lightbox').find('#spinnerLight').contents()[2].nodeValue = 'Espere un momento ...';
    var siniestro = $('#siniestro').val();
    var clave = $('#clave').val();
    var inciso = $('#inciso').val();

    var isValid = true;

    if (siniestro === '') {
        $('#Lightbox').hide();
        $('#lblErrorNumSiniestro').show();
    }
    else if (clave === '') {
        $('#Lightbox').hide();
        $('#lblErrorAsegurado').show();
    }
    else if (inciso === '') {
        $('#Lightbox').hide();
        $('#lblErrorInciso').show();
    }else {
        var encoded = encodeURI(window.location.href);
        //var uri = 'https://web5.abaseguros.com/acceso/AbrirServicioExpress.aspx?s=' + encoded + '&p=' + inciso + '&m=1&whr=214&np=' + siniestro + '&a=' + clave;
        var urlSiniestro = $('#urlSiniestro').val();
        var uri = urlSiniestro + encoded + '&p=' + inciso + '&m=1&whr=214&np=' + siniestro + '&a=' + clave;

        console.log(uri);
        window.location.replace(uri);
    }
});

$('#darSeguimiento').click(function (e) {
    $('#Lightbox').show();
    $('#Lightbox').find('#spinnerLight').contents()[2].nodeValue = 'Espere un momento ...';
    var siniestro = $('#siniestro').val();
    var clave = $('#clave').val();

    var isValid = true;

    if (siniestro === '') {
        $('#Lightbox').hide();
        $('#lblErrorNumSiniestro').show();
    }
    else if (clave === '') {
        $('#Lightbox').hide();
        $('#lblErrorAsegurado').show();
    }
    else {
        var urlSiniestro = $('#urlSiniestro').val();
        var uri = urlSiniestro + '214&m=1&whr=214&ns=' + siniestro + '&c=' + clave;
        console.log(uri);
        window.location.replace(uri);
    }
});

$('#siniestro').focus(function (e) {
    $('#lblErrorNumSiniestro').hide();
});

$('#clave').focus(function (e) {
    $('#lblErrorAsegurado').hide();
});

$('#inciso').focus(function (e) {
    $('#lblErrorInciso').hide();
});

var error = decodeURIComponent(utils.GetQueryStringParams('e')).split('+').join(' ');

if (error !== "undefined")
{
    $('#lblErrores .alert-warning > small').html(error);
    $('#lblErrores').show();
}