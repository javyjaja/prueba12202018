﻿document.getElementById("arefNuestro").addEventListener("click", funarefNuestro);
document.getElementById("arefLocalizador").addEventListener("click", funarefLocalizador);
document.getElementById("arefNuestro2").addEventListener("click", funarefNuestro2);
document.getElementById("arefNumero").addEventListener("click", funarefNumero);
document.getElementById("arefLocalizador2").addEventListener("click", funarefLocalizador2);
document.getElementById("arefMail").addEventListener("click", funarefMail);
document.getElementById("abrirChatLink").addEventListener("click", funabrirChatLink);
document.getElementById("btnEnviar").addEventListener("click", funbtnEnviar);

function funarefNuestro() {

    gtag('event', 'nuestro_contacto', {'event_category': 'contacto','event_label': ''});
}
function funarefLocalizador() {

   gtag('event', 'localizador_de_oficinas', {'event_category': 'contacto','event_label': ''});
}

function funarefNuestro2() {

    gtag('event', 'nuestro_contacto', {'event_category': 'contacto','event_label': ''});
}
function funarefNumero() {

    gtag('event', '01800', {'event_category': 'contacto','event_label': ''});
}
function funarefLocalizador2() {

    gtag('event', 'localizador_de_oficinas', { 'event_category': 'contacto', 'event_label': '' });
    
}
function funarefMail() {

    gtag('event', 'mail', {'event_category': 'contacto','event_label': ''});
}
function funabrirChatLink() {

    gtag('event', 'asesoria_en_linea', {'event_category': 'contacto','event_label': ''});
}
function funbtnEnviar() {

    gtag('event', 'nuestro_contacto', {'event_category': 'contacto','event_label': 'enviar'});
}