﻿
document.getElementById("btnOpenModalPagar").addEventListener("click", funbtnOpenModalPagar);
document.getElementById("btnAnterior").addEventListener("click", funbtnAnterior);
document.getElementById("btnPagar").addEventListener("click", funbtnPagar);
document.getElementById("btnNo").addEventListener("click", funbtnNo);

function funbtnOpenModalPagar() {

    gtag('event', 'paga_tu_seguro', {'event_category': 'hogar_cotizador','event_label': 'pagar'});
}
function funbtnAnterior() {

    history.go(-1);
    gtag('event', 'paga_tu_seguro', { 'event_category': 'hogar_cotizador', 'event_label': 'regresar' });
}
function funbtnPagar() {

    gtag('event', 'contratar_y_pagar', {'event_category': 'Formulario_de_pago','event_label': 'si'});
}
function funbtnNo() {

    gtag('event', 'contratar_y_pagar', {'event_category': 'Formulario_de_pago','event_label': 'no'});
}