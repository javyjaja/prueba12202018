﻿var imprimirResumenErrores = function (errores) {
    $("#pnlErrorCaptura").load("/Contenidos/ListadoErrores_", {
        errores: errores
    });
    $("#pnlErrorCaptura").show();
};

var validaEmail = function (email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

var validarFormulario = function () {
    var errores = [];
    var esValido = true;
    $('.has-error').removeClass('has-error');
    if ($('#Telefono').val() === '' /*|| $('#TelefonoCasa').val() === ''*/) {
        $('#Telefono').parent().attr("class", "control-container has-error");
        //$('#TelefonoCasa').parent().attr("class", "control-container has-error");

        errores.push({
            detalle: 'El campo [' + $('#Telefono').attr("placeholder").replace('*', '') + '] es requerido.' /*y [' + $('#TelefonoCasa').attr("placeholder").replace('*', '') + '] se requieren.'*/
        });

        esValido = false;
    }

    if ($('#Telefono').val() != '' && $('#Telefono').val().length != 10) {
        $('#Telefono').parent().attr("class", "control-container has-error");

        errores.push({
            detalle: 'El campo [' + $('#Telefono').attr("placeholder").replace('*', '') + '] debe de ser de 10 dígitos.'
        });

        esValido = false;
    }

    if (!validaEmail($(".email").val())) {
        $(".email").parent().attr("class", "control-container has-error");
        errores.push({
            detalle: 'El campo [' + $(".email").attr("placeholder").replace('*', '') + '] no tiene un email válido.'
        });
        esValido = false;
    }

    if (!validaEmail($(".emailconfirm").val())) {
        $(".emailconfirm").parent().attr("class", "control-container has-error");
        errores.push({
            detalle: 'El campo [' + $(".emailconfirm").attr("placeholder").replace('*', '') + '] no tiene un email válido.'
        });
        esValido = false;
    }

    if ($(".email").val() != $(".emailconfirm").val()) {
        errores.push({
            detalle: 'Los campos [' + $(".email").attr("placeholder").replace('*', '') + '] y [' + $(".emailconfirm").attr("placeholder").replace('*', '') + '] deben de ser iguales.'
        });
        esValido = false;
    }

    $("input[required=required]").each(function () {
        if ($(this).val() === '') {
            $(this).parent().attr("class", "control-container has-error");

            errores.push({
                detalle: 'El campo [' + $(this).attr("placeholder").replace('*', '') + '] es requerido.'
            });

            esValido = false;
        }
    });

    $("select[required=required]").each(function () {
        if ($(this).val() === '-1') {
            $(this).parent().attr("class", "control-container has-error");

            errores.push({
                detalle: 'El campo [' + $(this).attr("placeholder").replace('*', '') + '] es requerido.'
            });

            esValido = false;
        }
    });
    if ($("#EmailComplementoFacturante").val().length > 0) {
        if (!validaEmail($("#EmailComplementoFacturante").val())) {
            $("#EmailComplementoFacturante").parent().attr("class", "control-container has-error");
            errores.push({
                detalle: 'El campo [' + $("#EmailComplementoFacturante").attr("placeholder").replace('*', '') + '] no tiene un email válido.'
            });
            esValido = false;
        }
    }
    if ($("#EmailFacturante").val().length > 0) {
        if (!validaEmail($("#EmailFacturante").val())) {
            $("#EmailFacturante").parent().attr("class", "control-container has-error");
            errores.push({
                detalle: 'El campo [' + $("#EmailFacturante").attr("placeholder").replace('*', '') + '] no tiene un email válido.'
            });
            esValido = false;
        }
    }
    if (esValido) {
        var dia = $("#Dia").val();
        var mes = $("#Mes").val();
        var año = $("#Anio").val();

        var fechaNacimiento = moment(año + mes + dia);

        if (!fechaNacimiento.isValid()) {
            errores.push({
                detalle: "La fecha de nacimiento es inválida."
            });
            esValido = false;
        } else {
            var edad = moment().diff(fechaNacimiento, 'years');
            if (edad < 18) {
                errores.push({
                    detalle: "Las personas menores de 18 años no pueden contratar la políza."
                });
                esValido = false;
            }
        }
    }

    if (esValido !== true) {
        imprimirResumenErrores(errores);
    }

    return esValido;
};

$(document).ready(function () {
    $('[name=TodasMedidasSeguridad]').click(function () {
        if ($(this).prop('checked') == true) {
            $('[name=MedidasSeguridad]').prop('checked', true);
            $('[name=MedidasSeguridad][value=3]').prop('checked', false);
            $(this).prop('disabled', true);
        }
    });

    $('[name=MedidasSeguridad][value!=3]').click(function () {
        if ($('[name=MedidasSeguridad][value!=3]:checked').length > 0) {
            $('[name=MedidasSeguridad][value=3]').prop('checked', false);
        }
        if ($('[name=MedidasSeguridad][value!=3]').length != $('[name=MedidasSeguridad][value!=3]:checked').length) {
            $('[name=TodasMedidasSeguridad]').prop('checked', false);
            $('[name=TodasMedidasSeguridad]').prop('disabled', false);
        } else {
            $('[name=TodasMedidasSeguridad]').prop('checked', true);
            $('[name=TodasMedidasSeguridad]').prop('disabled', true);
        }
    });

    $('[name=MedidasSeguridad][value=3]').click(function () {
        if ($(this).prop('checked') == true) {
            $('[name=MedidasSeguridad][value!=3]').prop('checked', false);
            $('[name=TodasMedidasSeguridad]').prop('checked', false);
            $('[name=TodasMedidasSeguridad]').prop('disabled', false);
        }
    });

    $("#Anio").on('change', function () {
        var $frm = $('#frmRfcHide');
        var nombre = $("#PrimerNombreFisica").val() + ' ' + $("#SegundoNombreFisica").val();
        var paterno = $("#ApellidoPaternoFisica").val();
        var materno = $("#ApellidoMaternoFisica").val();
        var dia = $("#Dia").val();
        var mes = $("#Mes").val();
        var anio = $("#Anio").val();

        $("#nombre_h").val(nombre);
        $("#paterno_h").val(paterno);
        $("#materno_h").val(materno);
        $("#dia_h").val(dia);
        $("#mes_h").val(mes);
        $("#anio_h").val(anio);

        $.post($frm.attr("action"), $frm.serialize(), function (response) {
            $("#RfcFisica").val(response);
            $("#RfcFisica").change();
        }, 'json');

    });

    $("#CP").autocomplete({
        source: function (request, response) {
            $.getJSON(apiRoot + "AutoCotizador/ObtenerLocalizacion", {
                cp: request.term,
            }, function (responseData) {
                response(responseData.Localizaciones);
            });
        },
        position: {
            collision: 'flip'
        },
        minLength: 5,
        select: function (event, ui) {
            $('#Municipio').val(ui.item.Municipio);
            $.getJSON(apiRoot + "AutoCotizador/ObtenerEstadoMunicipio", {
                Estado: ui.item.Estado,
                Municipio: ui.item.Municipio,
                Poblacion: ui.item.Poblacion
            }, function (responseData) {
                $('#Colonia').val(ui.item.Colonia);
                $('#CP').val(ui.item.CodigoPostal);
                $('#EstadoDomicilio').val(responseData.Estado.Nombre);
                $('#MunicipioText').val(responseData.Municipio.Nombre);
                $('#Poblacion').val(responseData.Poblacion.Nombre);

                $('#Calle').focus();
            });

            return false;
        },
        open: function () {
            $(this).autocomplete('widget').css('z-index', 10000);
            return true;
        }
    }).autocomplete('instance')._renderItem = function (ul, item) {
        return $('<li>').append('<div>' + item.Colonia + '</div>').appendTo(ul);
    };

    $("#CodigoPostalBien").autocomplete({
        source: function (request, response) {
            $.getJSON(apiRoot + "AutoCotizador/ObtenerLocalizacion", {
                cp: request.term,
            }, function (responseData) {
                response(responseData.Localizaciones);
            });
        },
        position: {
            collision: 'flip'
        },
        minLength: 5,
        select: function (event, ui) {
            $('#MunicipioBien').val(ui.item.Municipio);
            $.getJSON(apiRoot + "AutoCotizador/ObtenerEstadoMunicipio", {
                Estado: ui.item.Estado,
                Municipio: ui.item.Municipio,
                Poblacion: ui.item.Poblacion
            }, function (responseData) {
                $('#ColoniaBien').val(ui.item.Colonia);
                $('#CodigoPostalBien').val(ui.item.CodigoPostal);
                $('#EstadoBien').val(responseData.Estado.Nombre);
                $('#MunicipioBienText').val(responseData.Municipio.Nombre);
                $('#PoblacionBien').val(responseData.Poblacion.Nombre);

                $('#CalleBien').focus();
            });

            return false;
        },
        open: function () {
            $(this).autocomplete('widget').css('z-index', 10000);
            return true;
        }
    }).autocomplete('instance')._renderItem = function (ul, item) {
        return $('<li>').append('<div>' + item.Colonia + '</div>').appendTo(ul);
    };

    $('[name=DomicilioAseguradoCheck]').change(function () {
        $('#DomicilioAsegurado').val($('[name=DomicilioAseguradoCheck]').is(':checked'));
        if ($(this).prop('checked') == true) {
            $('#CodigoPostalBien').val($('#CP').val());
            $('#EstadoBien').val($('#EstadoDomicilio').val());
            $('#MunicipioBienText').val($('#MunicipioText').val());
            $('#MunicipioBien').val($('#Municipio').val());
            $('#PoblacionBien').val($('#Poblacion').val());
            $('#ColoniaBien').val($('#Colonia').val());
            $('#CalleBien').val($('#Calle').val());
            $('#NumeroExteriorBien').val($('#NumeroExterior').val());
            $('#NumeroInteriorBien').val($('#NumeroInterior').val());
            $('#CalleBien').attr('disabled', 'disabled');
            $('#NumeroExteriorBien').attr('disabled', 'disabled');
            $('#NumeroInteriorBien').attr('disabled', 'disabled');
        } else {
            $('#CodigoPostalBien').val('');
            $('#EstadoBien').val('');
            $('#MunicipioBienText').val('');
            $('#MunicipioBien').val('');
            $('#PoblacionBien').val('');
            $('#ColoniaBien').val('');
            $('#CalleBien').val('');
            $('#NumeroExteriorBien').val('');
            $('#NumeroInteriorBien').val('');
            $('#CalleBien').removeAttr('disabled');
            $('#NumeroExteriorBien').removeAttr('disabled');
            $('#NumeroInteriorBien').removeAttr('disabled');
        }
    });

    $('[name=TipoFacturanteCheck]').change(function () {
        $('#TipoFacturanteId').val(($('[name=TipoFacturanteCheck]').is(':checked') ? 1 : 0));
        if ($(this).prop('checked') == true) {
            var nombre = $('#PrimerNombreFisica').val() + ' ' + $('#SegundoNombreFisica').val() + ' ' + $('#ApellidoPaternoFisica').val() + ' ' + $('#ApellidoMaternoFisica').val()
            $('#NombreFacturante').val(nombre);
            $('#RFCFacturante').val($('#RfcFisica').val());
            $('#EmailFacturante').val($('#Correo').val());
            $('#NombreFacturante').attr('disabled', 'disabled');
            $('#RFCFacturante').attr('disabled', 'disabled');
            $('#EmailFacturante').attr('disabled', 'disabled');
        } else {
            $('#NombreFacturante').val('');
            $('#RFCFacturante').val('');
            $('#EmailFacturante').val('');
            $('#NombreFacturante').removeAttr('disabled');
            $('#RFCFacturante').removeAttr('disabled');
            $('#EmailFacturante').removeAttr('disabled');
        }
    });

    $('#NoPisosBien').change(function () {
        $('#NoNivelesBien').val($('#NoPisosBien').val());
    });

    $('#Telefono, #TelefonoCasa, #CP').on('keydown', function (e) {
        //var keyCode = ('which' in event) ? event.which : event.keyCode;

        //isNotWanted = (keyCode == 69 || keyCode == 101);
        //return !isNotWanted;
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || (e.keyCode >= 35 && e.keyCode <= 40)) {
            return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $('#btnGenerarPolizaCasa').click(function () {
        var formData = $('#frmCapturaDatosGenerales').serialize();
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: apiRoot + "CasaCotizador/GenerarUrlPagoPolizaV2",
            beforeSend: function () {
                $('#Lightbox').show();
            },
            complete: function () {
                $('#Lightbox').hide();
            },
            data: formData,
            success: function (data) {
                if (data.Validaciones.length == 0) {
                    location.href = data.UrlPagoPoliza;
                }
            },
        });
    });

    $("#btnAnterior").click(function () {
        history.back();
    });

    $("input[required=required], #Telefono, #TelefonoCasa").focus(function () {
        $(this).parent().attr("class", "control-container");
    });

    $("select[required=required]").focus(function () {
        $(this).parent().attr("class", "control-container");
    });

    $("#btnSiguiente").click(function () {
        $("#btnSiguiente").attr("disabled", "disabled");
        $('#Lightbox').show();
        $('#Lightbox').find('#spinnerLight').contents()[2].nodeValue = 'Espere un momento ...';
        if (validarFormulario()) {
            var $form = $('#frmCapturaDatosGenerales');
            var disabled = $form.find(':input:disabled').removeAttr('disabled');
            var serialized = $form.serialize();

            $.post($form.attr("action"), $form.serialize(), function (response) {
                disabled.attr('disabled', 'disabled');
                $('#Lightbox').hide();
                $("#spinDatosProcesando").hide();
                $("#btnSiguiente").removeAttr("disabled");
                if (response.OperacionExitosa === true) {
                    $("#DatosSolicitud").val(response.DatosSolicitud);
                    $("#frmResultadoSolicitud").submit();
                } else {
                    //var errores = [];

                    //imprimirResumenErrores(errores);
                }
            });
        } else {
            $("#btnSiguiente").removeAttr("disabled");
            $('#Lightbox').hide();
            $("#spinDatosProcesando").hide();
        }
    });

    $('#PrimerNombreFisica, #SegundoNombreFisica, #ApellidoPaternoFisica, #ApellidoMaternoFisica').keypress(function (e) {
        var regex = /^[a-zA-Z\s]+$/
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        } else {
            e.preventDefault();
            return false;
        }
    });

    $('#Calle').change(function () {
        if ($('[name=DomicilioAseguradoCheck]').is(':checked')) {
            $('#CalleBien').val($('#Calle').val());
        }
    });
    $('#NumeroExterior').change(function () {
        if ($('[name=DomicilioAseguradoCheck]').is(':checked')) {
            $('#NumeroExteriorBien').val($('#NumeroExterior').val());
        }
    });
    $('#NumeroInterior').change(function () {
        if ($('[name=DomicilioAseguradoCheck]').is(':checked')) {
            $('#NumeroInteriorBien').val($('#NumeroInterior').val());
        }
    });

    $('#PrimerNombreFisica, #SegundoNombreFisica, #ApellidoPaternoFisica, #ApellidoMaternoFisica').change(function () {
        if ($('[name=TipoFacturanteCheck]').is(':checked')) {
            var nombre = $('#PrimerNombreFisica').val() + ' ' + $('#SegundoNombreFisica').val() + ' ' + $('#ApellidoPaternoFisica').val() + ' ' + $('#ApellidoMaternoFisica').val()
            $('#NombreFacturante').val(nombre);
        }
    });

    $('#Correo').change(function () {
        if ($('[name=TipoFacturanteCheck]').is(':checked')) {
            var nombre = $('#PrimerNombreFisica').val() + ' ' + $('#SegundoNombreFisica').val() + ' ' + $('#ApellidoPaternoFisica').val() + ' ' + $('#ApellidoMaternoFisica').val()
            $('#EmailFacturante').val($('#Correo').val());
        }
    });

    $('#RfcFisica').change(function () {
        if ($('[name=TipoFacturanteCheck]').is(':checked')) {
            var nombre = $('#PrimerNombreFisica').val() + ' ' + $('#SegundoNombreFisica').val() + ' ' + $('#ApellidoPaternoFisica').val() + ' ' + $('#ApellidoMaternoFisica').val()
            $('#RFCFacturante').val($('#RfcFisica').val());
        }
    });
});