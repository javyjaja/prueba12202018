﻿document.getElementById("btnOpenModalPagar").addEventListener("click", funbtnOpenModalPagar);
document.getElementById("btnAnterior").addEventListener("click", funbtnAnterior);
document.getElementById("btnPagar").addEventListener("click", funbtnPagar);
document.getElementById("btnNo").addEventListener("click", funbtnNo);

function funbtnOpenModalPagar() {
    gtag('event', 'paga_tu_seguro', { 'event_category': 'pyme_cotizador', 'event_label': 'pagar' });
}
function funbtnAnterior() {
    history.go(-1); gtag('event', 'paga_tu_seguro', { 'event_category': 'pyme_cotizador', 'event_label': 'regresar' });
}
function funbtnPagar() {
    gtag('event', 'paga_tu_seguro', { 'event_category': 'pyme_cotizador', 'event_label': 'si' });
}
function funbtnNo() {
    gtag('event', 'paga_tu_seguro', { 'event_category': 'pyme_cotizador', 'event_label': 'no' });
}