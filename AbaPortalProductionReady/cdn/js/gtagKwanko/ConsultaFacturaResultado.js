﻿document.getElementById("downloadFacturaPDF").addEventListener("click", fundownloadFacturaPDF);
document.getElementById("downloadFacturaXML").addEventListener("click", fundownloadFacturaXML);
document.getElementById("enviarFactura").addEventListener("click", funenviarFactura);
document.getElementById("btnEnviarPoliza").addEventListener("click", funbtnEnviarPoliza);

function fundownloadFacturaPDF() {
    gtag('event', 'descarga', { 'event_category': 'consulta_factura', 'event_label': '' });
}

function fundownloadFacturaXML() {
    gtag('event', 'descarga', { 'event_category': 'consulta_factura', 'event_label': '' });
}

function funenviarFactura() {
    gtag('event', 'descarga', { 'event_category': 'consulta_factura', 'event_label': '' });
}

function funbtnEnviarPoliza() {
    gtag('event', 'icono_imprime', { 'event_category': 'consultapoliza', 'event_label': 'imprimir_enviar' });
}