﻿document.getElementById("telefono").addEventListener("click", funtelefono);
document.getElementById("dudasChat").addEventListener("click", fundudasChat);
document.getElementById("divContactar").addEventListener("click", fundivContactar);
document.getElementById("btnEnviarDatos").addEventListener("click", funbtnEnviarDatos);
document.getElementById("cancelar").addEventListener("click", funcancelar);
document.getElementById("email").addEventListener("click", funemail);

function funtelefono() {
    gtag('event', 'lead_gen', { 'event_category': 'hogar_clon', 'event_label': '1800' });
}
function fundudasChat() {
    gtag('event', 'lead_gen', { 'event_category': 'hogar_clon', 'event_label': 'chat' });
}
function fundivContactar() {
    gtag('event', 'lead_gen', { 'event_category': 'hogar_clon', 'event_label': 'te_llamamos' });
}
function funbtnEnviarDatos() {
    gtag('event', 'lead_gen', { 'event_category': 'hogar_clon', 'event_label': 'te_llamamos_enviar' });
}
function funcancelar() {
    gtag('event', 'nosotros_te_llamamos', { 'event_category': 'cotizador_Casa', 'event_label': 'cancelar' });
}
function funemail() {
    gtag('event', 'lead_gen', { 'event_category': 'hogar_clon', 'event_label': 'email' });
}