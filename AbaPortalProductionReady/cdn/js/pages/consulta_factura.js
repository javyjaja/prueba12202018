﻿$('#consultaPoliza').click(function (e) {
    $(this).find('.fa-spin').show();
    $('#lblErrorConsulta').slideUp();
    $('#Lightbox').show();
    $('#Lightbox').find('#spinnerLight').contents()[2].nodeValue = 'Espere un momento ...';
    var siniestro = $('#siniestro').val();
    var clave = $('#clave').val();
    var inciso = $('#inciso').val();

    if (siniestro === '') {
        $('#Lightbox').hide();
        $('#lblErrorNumSiniestro').show();
        $(this).find('.fa-spin').hide();
        return false;
    }
    else if (clave === '') {
        $('#Lightbox').hide();
        $('#lblErrorAsegurado').show();
        $(this).find('.fa-spin').hide();
        return false;
    }
    else if (inciso === '') {
        $('#Lightbox').hide();
        $('#lblErrorInciso').show();
        $(this).find('.fa-spin').hide();
        return false;
    }
    else {
        $.post(apiRoot + "ServiciosExpress/ConsultaFactura", $("#frmFormularioConsulta").serialize(),
            function (res) {
                $('#Lightbox').hide();
                $('#consultaPoliza').attr({ 'disabled': 'disabled' });
                if (res.Valido == 0) {
                    $('#lblErrorConsulta').html($('<small>').text(res.Mensaje));
                    $('#lblErrorConsulta').slideDown();
                } else {
                    $('#lblErrorConsulta').slideUp();
                    //var template = _.template($("#plantillaMostrarBusquedaPoliza").html());
                    //$.each(res.Url, function (index, url) {
                    //        polizaResult = {
                    //            Url: url
                    //        };
                    //        $("#embedContainer").html(template({ poliza: polizaResult }));
                    //});
                    $('#pagoPolizaContainer').animate({ paddingTop: "100px" }, 200);
                    $('#consultaWindow').slideUp(function () {
                        $('html,body').animate({
                            scrollTop: $("#embedContainer").offset().top - 100
                        },'slow');
                    });
                    $('#embedRegresar').unbind('click');
                    $('#embedDescargar').click(function () {
                        var url = $(this).attr('data-url');

                        $.ajax({
                            cache: false,
                            url: apiRoot + 'ServiciosExpress/ConsultaPolizaDescargar',
                            data: { UrlDescarga: url },
                            success: function (data) {
                                var response = data;
                                window.location = '/ServiciosExpress/Download?fileGuid=' + response.FileGuid
                                                  + '&filename=' + response.FileName;
                            }
                        })

                    });
                    $('#embedRegresar').click(function () {
                        $('#pagoPolizaContainer').animate({ paddingTop: "200px" }, 200);
                        $('#consultaWindow').slideDown(function () {
                            $("#embedContainer").html('');
                            $('html,body').animate({
                                scrollTop: $("#consultaWindow").offset().top - 100
                            },
        'slow');
                        });
                        
                    });
                    $('#embedEmail').click(function () {
                        if ($('#emailWindow').is(':visible')) {
                            $('#emailWindow').slideUp();
                        } else {
                            $('#emailWindow').slideDown();
                        }
                    });
                    $('#enviaCorreo').click(function () {
                        $('#ErrorEmail').slideUp();
                        var email = $('#emailConsultaPoliza').val();
                        if (email === '' || email === undefined) {
                            $('#ErrorEmail').html('Favor de ingresar un Correo Electrónico.').slideDown();

                            return false;
                        }

                        if (!/\S+@\S+\.\S+/.test(email)) {
                            $('#ErrorEmail').html('Favor de ingresar un Correo Electrónico valido.').slideDown();
                            return false;
                        }
                        var url = $(this).attr('data-url');
                        $(this).find('.fa-spin').show();
                        $.ajax({
                            type:'POST',
                            url: apiRoot + 'ServiciosExpress/EnviarPoliza',
                            data: { UrlDescarga: url, CorreoElectronio: email },
                            success: function (data) {

                                $('#enviaCorreo').find('.fa-spin').hide();
                                if (data.Response == false) {
                                    $('#ErrorEmail').html('Ocurrio un error al enviar el Correo.').slideDown();
                                } else {
                                    $('#ErrorEmail').html('').slideUp();
                                }
                            }
                        })
                    });
                }
        }).fail(function () {
            $('#containerPrint').hide();
        });
    }
});


$('#siniestro').focus(function (e) {
    $('#lblErrorNumSiniestro').hide();
});

$('#clave').focus(function (e) {
    $('#lblErrorAsegurado').hide();
});

$('#inciso').focus(function (e) {
    $('#lblErrorInciso').hide();
});

var error = decodeURIComponent(utils.GetQueryStringParams('e')).split('+').join(' ');

if (error !== "undefined") {
    $('#lblErrores .alert-warning > small').html(error);
    $('#lblErrores').show();
}