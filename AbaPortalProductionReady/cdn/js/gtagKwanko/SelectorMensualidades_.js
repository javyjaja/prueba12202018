﻿document.getElementById("btnMensual").addEventListener("click", funbtnMensual);
document.getElementById("btnTrimestral").addEventListener("click", funbtnTrimestral);
document.getElementById("btnSemestral").addEventListener("click", funbtnSemestral);
document.getElementById("btnAnual").addEventListener("click", funbtnAnual);

function funbtnMensual() {
    gtag('event', 'mensual', { 'event_category': 'autocotizador_Kwanco', 'event_label': '' });
}

function funbtnTrimestral() {
    gtag('event', 'trimestral', { 'event_category': 'autocotizador_Kwanco', 'event_label': '' });
}

function funbtnSemestral() {
    gtag('event', 'semestral', { 'event_category': 'autocotizador_Kwanco', 'event_label': '' });
}

function funbtnAnual() {
    gtag('event', 'anual', { 'event_category': 'autocotizador_Kwanco', 'event_label': '' });
}