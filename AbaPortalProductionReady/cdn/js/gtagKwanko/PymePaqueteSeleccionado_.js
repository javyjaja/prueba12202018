﻿document.getElementById("terminosCondiciones").addEventListener("click", funterminosCondiciones);
document.getElementById("CondicionesLink").addEventListener("click", funCondicionesLink);
document.getElementById("btnComprar").addEventListener("click", funbtnComprar);
document.getElementById("agenteCerca").addEventListener("click", funagenteCerca);


function funterminosCondiciones() {
    gtag('event', 'terminos_y_condiciones', { 'event_category': 'pyme_cotizador', 'event_label': '' });
}
function funCondicionesLink() {
    gtag('event', 'ver_mas', { 'event_category': 'pyme_cotizador', 'event_label': '' });
}
function funbtnComprar() {
    gtag('event', 'boton_comprar', { 'event_category': 'pyme_cotizador', 'event_label': '' });
}
function funagenteCerca() {
    gtag('event', 'encuentra_un_agente_cerca_de_ti', { 'event_category': 'pyme_cotizador', 'event_label': '' });
}