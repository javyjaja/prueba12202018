﻿$(function () {

    $("input[data-behavior=mustbechecked]").change(function () {
        $("#pnlErrorPago").hide();
    });

    $("#btnPagar").click(function () {
       
        $("#btnOpenModalPagar").attr('disabled', 'disabled');
        var isValid = true;
        $("input[data-behavior=mustbechecked]").each(function () {
            if (!$(this).is(":checked")) {
                isValid = false;
            }
        });

        if (!isValid) {
            $("#pnlErrorPago").html("Es necesario que indique  la aceptaci&oacute;n de los t&eacute;rminos y condiciones.");
            $("#pnlErrorPago").show();
            $("#btnOpenModalPagar").removeAttr('disabled');
        } else {
            $("#btnOpenModalPagar").find('i').show();
            $(document.body).css({ 'cursor': 'wait' });
            var datosPoliza = {
                Poliza: $("#NumeroPolizaFormato").val(),
                ClavePoliza: $("#ClavePoliza").val(),
                Monto: $("#MontoPrimerPagoFormato").val(),
                FechaVigencia: $("#FechaVigenciaFormato").val(),
                Nombre: $("#Nombre").val(),
                CorreoElectronico: $("#CorreoElectronico").val()
            };

            localStorage.setItem("DatosPoliza" + $("#PolizaId").val(), JSON.stringify(datosPoliza));

            console.log("Datos de confirmación:");
            console.log(JSON.stringify(datosPoliza));

            $.post(apiRoot + "Kwanko/GenerarUrlPagoPolizaV2",
                    $("#frmDatosPagoPoliza").serialize(), function (response) {
                        if (response.Validaciones.length == 0) {
                            location.href = response.UrlPagoPoliza;
                        } else {
                            $("#btnOpenModalPagar").find('i').hide();
                            var errores = response.Validaciones;
                            $("#pnlErrorCapturaPago").load("/Contenidos/ListadoErrores_", { errores: errores });
                            $("#pnlErrorCapturaPago").show();
                        }
                    });
            $(document.body).css({ 'cursor': 'default' });
        }
    });
});