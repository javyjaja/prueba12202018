﻿/// <reference path="~/cdn/aplicacion/utils.js" />

// Test via a getter in the options object to see if the passive property is accessed
var supportsPassive = false;
try {
    var opts = Object.defineProperty({}, 'passive', {
        get: function () {
            supportsPassive = true;
        }
    });
    window.addEventListener("testPassive", null, opts);
    window.removeEventListener("testPassive", null, opts);
} catch (e) { }

function remove_dash_agent_name(strAgentName) {
    var agentNameMessage = strAgentName.split('-')[1];

    if (!agentNameMessage) {
        return strAgentName;
    } else {
        return strAgentName.split('-')[1];
    }
}

function sleep(milliseconds) {
    var start = new Date().getTime();

    for (var i = 0; i < 7; i++) {
        if ((new Date().getTime - start) > milliseconds) {
            break;
        }
    }
}

//
// $('#element').donetyping(callback[, timeout=1000])
// Fires callback when a user has finished typing. This is determined by the time elapsed
// since the last keystroke and timeout parameter or the blur event--whichever comes first.
//   @callback: function to be called when even triggers
//   @timeout:  (default=1000) timeout, in ms, to to wait before triggering event if not
//              caused by blur.
// Requires jQuery 1.7+
//
; (function ($) {
    $.fn.extend({
        donetyping: function (callback, timeout) {
            timeout = timeout || 1e3; // 1 second default timeout
            var timeoutReference,
                doneTyping = function (el) {
                    if (!timeoutReference) return;
                    timeoutReference = null;
                    callback.call(el);
                };
            return this.each(function (i, el) {
                var $el = $(el);
                // Chrome Fix (Use keyup over keypress to detect backspace)
                // thank you @palerdot
                $el.is(':input') && $el.on('keyup keypress paste', function (e) {
                    // This catches the backspace button in chrome, but also prevents
                    // the event from triggering too preemptively. Without this line,
                    // using tab/shift+tab will make the focused element fire the callback.
                    if (e.type == 'keyup' && e.keyCode != 8) return;

                    // Check if timeout has been set. If it has, "reset" the clock and
                    // start over again.
                    if (timeoutReference) clearTimeout(timeoutReference);
                    timeoutReference = setTimeout(function () {
                        // if we made it here, our timeout has elapsed. Fire the
                        // callback
                        doneTyping(el);
                    }, timeout);
                }).on('blur', function () {
                    // If we can, fire the event since we're leaving the field
                    doneTyping(el);
                });
            });
        }
    });
})(jQuery);



function Chat(settings) {
    var context = this;

    this.localStorage = window.localStorage;

    this.ChatClosedIntervalReference = 0;
    this.ChatClosed = false;
    this.ChatCloseTimeoutReference = 0;
    this.IsChatHistoryAvailable = (function () {
        var conversation = context.localStorage.getItem('chat_aba');

        if (conversation === null || conversation === '') {
            return false;
        }

        return true;
    }());

    this.IsChatOnSession = (function () {
        var conversation = context.localStorage.getItem('chat_aba_on_session');

        if (conversation === null || conversation === '') {
            return false;
        }

        return true;
    }());

    this.setIsChatOnSession = function () {
        this.localStorage.setItem('chat_aba_on_session', '1');
    };

    this.clearIsChatOnSession = function () {
        this.localStorage.removeItem('chat_aba_on_session');
    };

    this.IsChatAlreadyOpened = (function () {
        var isAlreadyOpened = context.localStorage.getItem('chat_aba_opened');

        if (isAlreadyOpened === null || isAlreadyOpened === '') {
            return false;
        }

        return true;
    }());

    this.IsChatOnSession = (function () {
        var isOnSession = context.localStorage.getItem('chat_aba_on_session');

        if (isOnSession === null || isOnSession === '') {
            return false;
        }

        return true;
    }());

    this.IsChatToggle = (function () {
        var isToggle = context.localStorage.getItem('chat_aba_toggle');

        if (isToggle === null || isToggle === '') {
            return false;
        }

        return true;
    }());

    this.clearIsChatToggle = function () {
        this.localStorage.removeItem('chat_aba_toggle');
    };

    this.setChatAlreadyOpened = function () {
        this.localStorage.setItem('chat_aba_opened', '1');
    };


    this.setChatOpenedToggle = function () {
        this.localStorage.setItem('chat_aba_toggle', '1');
    };

    this.clearChatAlreadyOpened = function () {
        this.localStorage.removeItem('chat_aba_opened');
        this.localStorage.removeItem('chat_aba_toggle');
    };

    

    // Get Chat Settings
    this.ChatSettings = (function () {
        var chatSettings = this.localStorage.getItem('chat_aba_settings');

        if(chatSettings === null || chatSettings === '')
        {
            return {};
        }

        return JSON.parse(chatSettings);
    }());
    
    var scrollDown = function () {
        $("#conversationContainer").scrollTop($("#conversationContainer")[0].scrollHeight);
    };

    // Get Conversation History from Local Storage
    this.getConversationFromStorage = function () {
        var conversation = this.localStorage.getItem('chat_aba');

        if (conversation === null || conversation === '') {
            return [];
        }

        return JSON.parse(conversation);
    };

    // Set Conversation History from Local Storage
    this.setConversationtoStorage = function (conversation) {
        this.localStorage.setItem('chat_aba', JSON.stringify(conversation));
    };

    this.setChatSettings = function (settings) {
        this.localStorage.setItem('chat_aba_settings', JSON.stringify(settings));
        this.ChatSettings = settings;
    };

    this.cleanLocalStorage = function () {
        this.localStorage.removeItem('chat_aba_settings');
        this.localStorage.removeItem('chat_aba');
    }

    this.Conversation = this.getConversationFromStorage() || [];

    this.RenderConversation = function () {
        var context = this;
        $("#conversationContainer").load("/Chat/ObtenerConversacion_", { conversacion: context.Conversation });
        scrollDown();
    };

    this.ReportarFinTecleo = function () {
        var context = this;
        $.post(webSiteRoot + "Chat/StopTyping", {
            Tenant: context.ChatSettings.tenantName,
            UserId: context.ChatSettings.userId,
            SecureKey: context.ChatSettings.secureKey,
            Alias: context.ChatSettings.alias,
            Path: context.ChatSettings.path
        }, function (response) {
            this.UsuarioTecleando = false;
            console.log('Fin tecleo');
        });
    };

    this.ReportarInicioTecleo = function () {
        var context = this;

        $.post(webSiteRoot + "Chat/StartTyping", {
            Tenant: context.ChatSettings.tenantName,
            UserId: context.ChatSettings.userId,
            SecureKey: context.ChatSettings.secureKey,
            Alias: context.ChatSettings.alias,
            Path: context.ChatSettings.path
        }, function (response) {
            this.UsuarioTecleando = true;
            console.log('Inicio tecleo');
        });
    };



    this.Initialize = function () {
        var context = this;
        $('#chatCollapsible').on('shown.bs.collapse', function () {
            $("#collapsibleGlyph").removeClass("glyphicon-plus").addClass("glyphicon-minus");
        });

        $('#chatCollapsible').on('hidden.bs.collapse', function () {
            $("#collapsibleGlyph").removeClass("glyphicon-minus").addClass("glyphicon-plus");
        });

        $("#btnChatEnviarMensaje").click(function () {
            context.EnviarMensaje($("#txtChatMensaje").val());
        });

        $('#closeChat').click(function (e) {
            e.preventDefault();

            var $closedChatPanel = $("#chatCerradoContainer");
            var $endedChatPanel = $("#chatFinalizadoContainer");
            var $chatPanel = $('#chatControlContainer');
            var $chatErrorPanel = $('#chatErrorContainer');

            if ($chatErrorPanel.is(':visible')) {
                $chatErrorPanel.hide();
            }

            if ($closedChatPanel.is(':visible')) {
                $closedChatPanel.hide();
            }

            if ($endedChatPanel.is(':visible')) {
                $endedChatPanel.hide();
            }

            if ($chatPanel.is(':visible')) {
                $chatPanel.hide();
            }
            $("#btnStopChat").hide();
            context.UsuarioTecleando = false;

            $('#pnlUserTyping').html('');

            $('#chatPanel').hide();

            context.Stop();
            context.clearChatAlreadyOpened();
            context.clearIsChatOnSession();

        })

        $("#txtChatMensaje").keypress(function (e) {
            if (e.keyCode === 13) {
                context.EnviarMensaje($("#txtChatMensaje").val());
            }
            if (context.UsuarioTecleando !== true) {
                context.ReportarInicioTecleo();
            }
        });

        $("#btnStopChat").click(function () {
            $(this).hide();
            context.Stop();

            var $endedChatPanel = $('#chatFinalizadoContainer');
            var $chatPanel = $('#chatControlContainer');
            var $chatErrorPanel = $('#chatErrorContainer');

            if ($chatErrorPanel.is(':visible')) {
                $chatErrorPanel.hide();
            }

            if (!$endedChatPanel.is(':visible')) {
                $endedChatPanel.show();
            }

            if ($chatPanel.is(':visible')) {
                $chatPanel.hide();
            }

            context.clearChatAlreadyOpened();
            context.clearIsChatOnSession();
            context.UsuarioTecleando = false;
            $('#pnlUserTyping').html('');
        });

        $('#txtChatMensaje').donetyping(function () {
            context.ReportarFinTecleo();
        }, 3000);


        $('#chatHeader').click(function (e) {
            $('#chatCollapsible').collapse('toggle');
            if ($('#chatCollapsible').attr('aria-expanded') == "false") context.setChatOpenedToggle();
            else context.clearIsChatToggle();
        });
    };

    this.Settings = settings;

    this.Start = function (onComplete, onFail) {
        var context = this;
        $.post(webSiteRoot + "Chat/Connect", $("#frmChatStart").serialize(), function (response) {
            try {
                var ChatSettings = JSON.parse(response);
                ChatSettings.TranscriptPosition = 1;
                context.setChatSettings(ChatSettings);

                onComplete();

                context.Escuchando = true;
                context.EscucharMensajes();
            } catch (e) {
                onFail(response);
            }
        });
    };

    this.ResetView = function () {
        $("#chatFormularioInicio").show();
        $("#btnStopChat").hide();
        $("#chatControlContainer").hide();
       // $("#chatPanel").hide();
    };

    this.ReloadChatWindow = function () {
        this.Escuchando = true;

        // Reload Chat Messages
        context.RenderConversation();


        this.EscucharMensajes();
    };

    this.Stop = function () {
        var context = this;
        console.log('Se finalizó el chat.');
        this.Escuchando = false;

        var context = this;
        $.post(webSiteRoot + "Chat/Disconnect", {
            Tenant: context.ChatSettings.tenantName,
            UserId: context.ChatSettings.userId,
            SecureKey: context.ChatSettings.secureKey,
            Alias: context.ChatSettings.alias,
            Path: context.ChatSettings.path
        }, function (response) {
            console.log('Fin de sesión de chat.');
            context.Conversation = [];
            context.cleanLocalStorage();
            context.RenderConversation();
        });
    };

    this.Sender = 'ABA SEGUROS';

    this.Escuchando = false;

    this.UsuarioTecleando = false;

    this.EscucharMensajes = function () {
        var in_out = "";
        var refresh = false;
        var context = this;
        var mensajesTimeout = setTimeout(function () {
            if (context.Escuchando) {
                $.post(webSiteRoot + "Chat/ObtenerMensajes", {
                    Tenant: context.ChatSettings.tenantName,
                    Message: $("#txtChatMensaje").val(),
                    UserId: context.ChatSettings.userId,
                    SecureKey: context.ChatSettings.secureKey,
                    Alias: context.ChatSettings.alias,
                    Path: context.ChatSettings.path,
                    TranscriptPosition: context.ChatSettings.TranscriptPosition
                }, function (response) {
                    var data = JSON.parse(response);

                    refresh = false;
                    context.ChatSettings.TranscriptPosition = data.nextPosition
                    $.each(data.messages, function (index, message) {

                        switch (message.type) {
                            case 'Message':
                                in_out = "incoming";
                                if (message.from.type === 'Client') {
                                    in_out = "outgoing";
                                }

                                context.Conversation.push({
                                    messageId: 1,
                                    sender: remove_dash_agent_name(message.from.nickname),
                                    text: message.text.replace(/\n/g, "<br />"),
                                    type: in_out,
                                    msgTime: moment(message.utcTime).local().format("DD/MM/YYYY hh:mm a")
                                });

                                $("#pnlUserTyping").html("");

                                refresh = true;
                                break;
                            case 'ParticipantLeft':
                                if ((message.from.type === 'Agent') || (message.from.type === 'Supervisor')) {
                                    context.Conversation.push({
                                        messageId: 1,
                                        sender: "ABA",
                                        text: remove_dash_agent_name(message.from.nickname) + ' se desconectó',
                                        type: 'incoming',
                                        msgTime: moment(message.utcTime).local().format("DD/MM/YYYY hh:mm a")
                                    });

                                    refresh = true;
                                }

                                break;

                            case 'ParticipantJoined':
                                if ((message.from.type === 'Agent') || (message.from.type === 'Supervisor')) {
                                    context.Conversation.push({
                                        messageId: 1,
                                        sender: "ABA",
                                        text: 'Gracias por contactarnos, le atiende ' + remove_dash_agent_name(message.from.nickname),
                                        type: 'incoming',
                                        msgTime: moment(message.utcTime).local().format("DD/MM/YYYY hh:mm a")
                                    });
                                    refresh = true;
                                }

                                break;

                            case 'TypingStarted':
                                if (message.from.type !== 'Client') {
                                    $("#pnlUserTyping").html(remove_dash_agent_name(message.from.nickname) + " est&aacute; escribiendo...")
                                }
                                break;
                            case 'TypingStopped':
                                if (message.from.type !== 'Client') {
                                    $("#pnlUserTyping").html("")
                                }
                                break;

                            case 'Notice':
                                if ((message.from.type === 'Agent') || (message.from.type === 'Supervisor')) {
                                    context.Conversation.push({
                                        messageId: 1,
                                        sender: remove_dash_agent_name(message.from.nickname),
                                        text: message.text.replace("\n", "<br/>"),
                                        type: 'incoming',
                                        msgTime: moment(message.utcTime).local().format("DD/MM/YYYY hh:mm a")
                                    });

                                    refresh = true;
                                }
                                break;
                        }

                        console.log(JSON.stringify(message));

                        // Set Conversation Context to the LocalStorage
                        context.setConversationtoStorage(context.Conversation);
                    });

                    if (data.chatEnded === true) {
                        sleep(6000); // Pon a Dormir 6 Segundos
                        $('#chatFinalizadoContainer').show();
                        $('#chatFormularioInicio').hide();
                        $('#CollapseChatbutton').hide();
                        $('#chatControlContainer').hide();
                        $('#btnStopChat').hide();
                        context.Stop();
                        clearTimeout(mensajesTimeout);
                        context.clearChatAlreadyOpened();
                        context.clearIsChatOnSession();
                        context.UsuarioTecleando = false;
                    }

                    if (refresh === true) {
                        context.RenderConversation();
                    }
                });
            }

            if (context.Escuchando) {
                context.EscucharMensajes();
            }
        }, 750);
    };

    this.EnviarMensaje = function (message) {
        var context = this;

        if ($("#txtChatMensaje").val().length > 0) {
            $.post(webSiteRoot + "Chat/SendMessage", {
                Tenant: context.ChatSettings.tenantName,
                Message: $("#txtChatMensaje").val(),
                UserId: context.ChatSettings.userId,
                SecureKey: context.ChatSettings.secureKey,
                Alias: context.ChatSettings.alias,
                Path: context.ChatSettings.path
            }, function (response) {

                $("#txtChatMensaje").val("");
                $("#txtChatMensaje").focus();
                context.RenderConversation();
            });
        }
    };

    this.ValidateFields = function (onSuccessCallback, onFailureCallback) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        

        if ($('#Name').val() === '') {
            onFailureCallback('#lblErrorChatNombre', 'El campo Nombre no puede estar vacio');
            return;
        }

        if ($('#Email').val() === '') {
            onFailureCallback('#lblErrorChatEmail', 'El campo Email no puede estar vacio');
            return;
        }

        if ($('#Subject').val() === '') {
            onFailureCallback('#lblErrorChatNecesidad', 'Es necesario que seleccione su necesidad');
            return;
        }

        if (re.test($('#Email').val()) == false) {
            onFailureCallback('#lblErrorChatEmail', 'El Email es incorrecto');
            return;
        }

        onSuccessCallback();

    };

    this.HideValidationError = function () {
        $('#lblErrorChatNombre').hide();
        $('#lblErrorChatEmail').hide();
        $('#lblErrorChatNecesidad').hide();
    }

    this.Initialize();


    (function () {
        var func = function () {
            $.ajax({
                type: 'POST',
                url: webSiteRoot + "Chat/EsChatcerrado",
                success: function (response) {
                    context.ChatClosed = response.ChatCerrado;
                    var $closedChatPanel = $("#chatCerradoContainer");
                    var $endedChatPanel = $("#chatFinalizadoContainer");
                    var $chatFormularioInicio = $('#chatFormularioInicio');

                    if (context.ChatClosed) {
                        $closedChatPanel.show();
                        $chatFormularioInicio.hide();
                        context.cleanLocalStorage();
                        //$('#chatCollapsible').removeClass('in');
                    }
                    else {
                        $closedChatPanel.hide();
                        $endedChatPanel.hide();
                        $('#chatErrorContainer').hide();
                        if (!localStorage.getItem('chat_aba_on_session')) $chatFormularioInicio.show();
                        context.setChatAlreadyOpened();
                        //$('#chatCollapsible').addClass('in');
                    }

                },
                dataType: 'json',
                async: true
            });
        };
        func();

        context.ChatClosedIntervalReference = setInterval(func, 10000);

        var interValShowChat = setInterval(function () { showChat() }, 10000);

        function showChat() {
            if (!context.IsChatOnSession) {
                if ($("#chatPanel").is(':visible') == false){
                    $('#chatCollapsible').collapse('hide');
                $("#chatPanel").show();
                context.setChatOpenedToggle();
                }
            }
            stopIntervalShowChat();
        }

        function stopIntervalShowChat() {
            clearInterval(interValShowChat);
        }
    }());
};

$(function () {
    var chat = new Chat();

    $('#CollapseChatbutton').click(function (e) {
        if($('#lblHelp').hasClass('hidden'))
        {
            $('#lblHelp').removeClass('hidden');
        } else {
            $('#lblHelp').addClass('hidden');
        }
    });

    if (chat.IsChatAlreadyOpened && chat.IsChatOnSession) {
        chat.Escuchando = true;
        chat.EscucharMensajes();

        $("#chatPanel").show().collapse('show');
        $("#chatFormularioInicio").hide();
        $("#btnStopChat").show();
        $("#chatControlContainer").show();
        $('#chatCerradoContainer').hide();
    }

    if (chat.IsChatAlreadyOpened && chat.ChatClosed) {
        $('#chatCerradoContainer').show();
    }

    if (chat.IsChatAlreadyOpened) {
        if (chat.IsChatToggle && ($('#chatCollapsible').attr('aria-expanded') == "true" || $('#chatCollapsible').attr('aria-expanded') == undefined)) { $('#chatCollapsible').collapse('toggle'); }

        $("#chatPanel").show().collapse('show');
        //$('#chatCerradoContainer').hide();
    }

    $("#btnIniciarChat").click(function (e) {
        e.preventDefault();

        chat.HideValidationError();

        // Validate the fields
        chat.ValidateFields(function () {
            // On Success Start Chat
            chat.Start(function () {
                $("#chatFormularioInicio").hide();
                $("#btnStopChat").show();
                $("#chatControlContainer").show();
                $('#chatCerradoContainer').hide();
                chat.setIsChatOnSession();
            }, function (e) {
                $("#chatFormularioInicio").hide();
                $('#chatErrorContainer').show();
                console.log(e);
            });
        }, function (selector, message) {
            // On Error, mark field
            //$("#btnStopChat").trigger('click');
            $(selector).text(message);
            $(selector).show();
        });
    });

    $("#lnkLanzarChat").click(function (e) {
        e.preventDefault();
        var $closedChatPanel = $("#chatCerradoContainer");
        var $endedChatPanel = $("#chatFinalizadoContainer");
        var $chatFormularioInicio = $('#chatFormularioInicio');

        if (chat.ChatClosed) {
            $closedChatPanel.show();
            $endedChatPanel.hide();
            $chatFormularioInicio.hide();
            chat.cleanLocalStorage();
        }
        else {
            $closedChatPanel.hide();
            $endedChatPanel.hide();
            $chatFormularioInicio.show();
            chat.setChatAlreadyOpened();
        }
        
        $("#chatPanel").show().collapse('show');

        return false;
    });   
});