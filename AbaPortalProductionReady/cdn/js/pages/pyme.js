﻿

var periodicidad = "trimestral";

var imprimirPaqueteSeleccionado = function (paqueteId) {
    switch (paqueteId) {
        case "1":
            descripcion = "Basico";
            monto = "$8,315.00 MN";
            montoUnformat = 8315;
            break;
        case "2":
            descripcion = "Amplia";
            monto = "$10,966.00 MN";
            montoUnformat = 10966;
            break;
        case "3":
            descripcion = "Integral";
            monto = "$11,392.00 MN";
            montoUnformat = 11392;
            break;
    }

    switch (periodicidad) {
        case 'mensual':
            formaPago = "Forma de pago: Mensual";
            primerPago = "Primer pago: $2550.00 MN";
            pagoSubsecuente = "11 pagos subsecuentes: $" + parseInt((montoUnformat - 2550) / 11) + ".00";
            break;
        case 'trimestral':
            formaPago = "Forma de pago: Trimestral";
            primerPago = "Primer pago: $4550.00 MN";
            pagoSubsecuente = "2 pagos subsecuentes: $" + parseInt((montoUnformat - 4550) / 2) + ".00";
            break;
        case 'semestral':
            formaPago = "Forma de pago: Semestral";
            primerPago = "Primer pago: $6550.00 MN";
            pagoSubsecuente = "1 pago subsecuente de: $" + parseInt(montoUnformat - 6550) + ".00";
            break;
        case 'anual':
            formaPago = "Forma de pago: Anual";
            primerPago = "Primer pago: $" + monto;
            pagoSubsecuente = "1 único pago de: " + monto;
            break;
    }

    $("[data-Descripcion]").html(descripcion);
    $("[data-Monto]").html(monto);
    $("[data-FormaPago]").html(formaPago);
    $("[data-PrimerPago]").html(primerPago);
    $("[data-PagoSubsecuente]").html(pagoSubsecuente);

    var vigencia = moment().format("L");
    $("[data-Vigencia]").html(vigencia);

    $('#pnlSeleccionPaquete .descripcion').html(descripcion);
    $('#pnlSeleccionPaquete .monto').html(monto);
};

$("a[data-action=seleccionarPaquete]").click(function () {
    $("a[data-action=seleccionarPaquete]").parent().removeClass('selected');
    $(this).parent().addClass('selected');

    var paqueteId = $(this).attr("data-paqueteid");
    imprimirPaqueteSeleccionado(paqueteId);
    $('#pnlSeleccionPaquete').removeClass('hidden');

    if ($(window).width() < 1024) {
        if (paqueteId != 2) {
            $('#pnlSeleccionPaquete .opcion-recomendada-header').hide();
            $('#pnlSeleccionPaquete .opcion-recomendada-star').hide();
        }
        else {
            $('#pnlSeleccionPaquete .opcion-recomendada-header').show();
            $('#pnlSeleccionPaquete .opcion-recomendada-star').show();
        }

        $('.paquete').addClass('hidden');
        $('#pnlMensualidades .mensualidades').addClass('hidden');
    }

    return false;
});


$("button[data-action=periodicidad]").click(function () {
    $("button[data-action=periodicidad]").attr("class", "btn btn-bordered");
    $(this).attr("class", "btn btn-bordered-active");

    periodicidad = $(this).attr("data-periodicidad");

    imprimirPaqueteSeleccionado();
});


$("#btnComparaCoberturas").click(function () {
    $('.paquete').addClass('hidden');
    $('#pnlSeleccionPaquete').addClass('hidden');
    $('#pnlMensualidades').addClass('hidden');
    $('#tblComparativa').removeClass('hidden');
});

$("#tblComparativa #btnRegresar").click(function () {
    $('.paquete').removeClass('hidden');
    $('#pnlMensualidades').removeClass('hidden');

    if ($(".paquete .itemCotizacion").hasClass('selected')) {
        $('#pnlSeleccionPaquete').removeClass('hidden');
    }

    $('#tblComparativa').addClass('hidden');
});


$("#btnCerrar").click(function () {
    $('#pnlSeleccionPaquete').addClass('hidden');
    $(".paquete .itemCotizacion").removeClass('selected')

    $('.paquete').removeClass('hidden');
    $('#pnlMensualidades .mensualidades').removeClass('hidden');
    goToByScroll('seleccionaPaquete');
    //clear selection
});



function goToByScroll(id) {
    // Remove "link" from the ID
    id = id.replace("link", "");
    // Scroll
    $('html,body').animate({
        scrollTop: $("#" + id).offset().top
    },
        'slow');
}

$("a").click(function () {
    idToGo = $(this).attr("href").replace("#", "");
    goToByScroll(idToGo);
});

$("#btnCotizar").click(function () {
    $('#seleccionaPaquete').removeClass('hidden');

    goToByScroll('seleccionaPaquete');
});

$("#btnComprar").click(function () {
    $('#capturarDatos').removeClass('hidden');

    goToByScroll('capturarDatos');
});

$(".capturarDatos #btnAnterior").click(function () {
    goToByScroll('seleccionaPaquete');
});

$("#btnSiguiente").click(function () {
    $('#pnlFormularioPago').removeClass('hidden');

    goToByScroll('pnlFormularioPago');
});

$("#pnlFormularioPago #btnAnterior").click(function () {
    goToByScroll('capturarDatos');
});

$("#btnPagar").click(function () {
    $('#pnlConfirmacionPoliza').removeClass('hidden');

    goToByScroll('pnlConfirmacionPoliza');
});


$('#btnCotizarPyme').click(function () {
    $('#pnlErrorCotizadorPyme').slideUp();
    if ($('#GiroId').val() != "" && $('#SubGiroId').val() != "" && $('#PerfilIdPyme').val() != "-1" && $('#CPPyme').val() != "") {
        var subgiroSeleccionado = "";
        if ($('#Subgiros').is(':visible')) subgiroSeleccionado = $('#Subgiros').val();
        else subgiroSeleccionado = $("#cmbSubgiros option:selected").text();
        localStorage.setItem('cbxSubGiroId', subgiroSeleccionado);
        localStorage.setItem('cbxPyme', $("#PerfilIdPyme option:selected").text());
        $('#frmCotizadorPyme').submit();
    } else {
        $('#pnlErrorCotizadorPyme').slideDown();
    }

});

$('#CPPyme').on('keydown', function (e) {
    //var keyCode = ('which' in event) ? event.which : event.keyCode;

    //isNotWanted = (keyCode == 69 || keyCode == 101);
    //return !isNotWanted;
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || (e.keyCode >= 35 && e.keyCode <= 40)) {
        return;
    }
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

//$("#CPPyme").autocomplete({
//    source: function (request, response) {
//        $.getJSON(apiRoot + "AutoCotizador/ObtenerLocalizacion", {
//            cp: request.term,
//        }, function (responseData) {
//            response(responseData.Localizaciones);
//        });
//    },
//    position: { collision: 'flip' },
//    minLength: 5,
//    select: function (event, ui) {
//        $('#CPPyme').val(ui.item.CodigoPostal);

//        return false;
//    },
//    open: function () {
//        $(this).autocomplete('widget').css('z-index', 10000);
//        return true;
//    }
//}).autocomplete('instance')._renderItem = function (ul, item) {
//    return $('<li>').append('<div>' + item.Colonia + '</div>').appendTo(ul);
//};

$("#Subgiros").autocomplete({
    source: function (request, response) {
        $.getJSON(apiRoot + "PymeCotizador/BuscarSubgiros", {
            busqueda: request.term,
        }, function (responseData) {
            if (responseData.items.length == 0) {
                $('#pnlErrorSubgirosPyme').slideDown();
                $('#SubGiroId').val('');
                $('#GiroId').val('');
            }
            else $('#pnlErrorSubgirosPyme').slideUp();
            response(responseData.items);
        });
    },
    position: { collision: 'flip' },
    minLength: 2,
    select: function (event, ui) {
        $('#Subgiros').val(ui.item.label);
        $('#SubGiroId').val(ui.item.id);
        $('#GiroId').val(ui.item.parentid);

        return false;
    },
    open: function () {
        $(this).autocomplete('widget').css('z-index', 10000);
        return true;
    }
}).autocomplete('instance')._renderItem = function (ul, item) {
    return $('<li>').append('<div>' + item.label + '</div>').appendTo(ul);
};

buscarGiros = function () {
    $.getJSON(apiRoot + "PymeCotizador/BuscarSubgiros", {

    }, function (responseData) {
        $('#cmbSubgiros').append($("<option>").val(-1).text("Tu giro es:"));
        $.each(responseData.items, function (index, item) {
            $('#cmbSubgiros').append($("<option>").val(item.id).text(item.label).data('GiroId', item.parentid));
        });
        $('#cmbSubgiros').append($("<option>").val(0).text("Otro"));
    });
}
buscarGiros();
$('#cmbSubgiros').change(function () {
    if ($(this).val() == 0) {
        $('#Subgiros').slideDown();
    } else {
        $('#Subgiros').slideUp();
        $('#GiroId').val($(this).find('option:selected').data('GiroId'));
        $('#SubGiroId').val($(this).val());
    }
});