﻿$('#payFirstStep').click(function (e) {
    $('#Lightbox').show();
    $('#Lightbox').find('#spinnerLight').contents()[2].nodeValue = 'Espere un momento ...';
    var siniestro = $('#siniestro').val();
    var clave = $('#clave').val();
    var inciso = $('#inciso').val();

    if (siniestro === '') {
        $('#Lightbox').hide();
        $('#lblErrorNumSiniestro').show();
    }
    else if (clave === '') {
        $('#Lightbox').hide();
        $('#lblErrorAsegurado').show();
    }
    else if (inciso === '') {
        $('#Lightbox').hide();
        $('#lblErrorInciso').show();
    }
    else {
        // Si existe un error, va a llevar un parametro adicional a la url del parametro s, llamado e, con la descripcion del error.
        var encoded = encodeURI(window.location.href);
        //var uri = 'https://web5.abaseguros.com/acceso/AbrirServicioExpress.aspx?s=' + encoded + '&p='+ inciso +'&m=3&whr=214&np=' + siniestro + '&a=' + clave;
        var urlSiniestro = $('#urlSiniestro').val();
        var uri = urlSiniestro + encoded + '&p=' + inciso + '&m=3&whr=214&np=' + siniestro + '&a=' + clave;
        console.log(uri);

        window.location.replace(uri);
    }
});

$('#siniestro').focus(function (e) {
    $('#lblErrorNumSiniestro').hide();
});

$('#clave').focus(function (e) {
    $('#lblErrorAsegurado').hide();
});

$('#inciso').focus(function (e) {
    $('#lblErrorInciso').hide();
});

var error = decodeURIComponent(utils.GetQueryStringParams('e')).split('+').join(' ');

if (error !== "undefined") {
    $('#lblErrores .alert-warning > small').html(error);
    $('#lblErrores').show();
}