﻿document.getElementById("embedDescargar").addEventListener("click", funembedDescargar);
document.getElementById("embedEnviarCorreo").addEventListener("click", funembedEnviarCorreo);
document.getElementById("btnEnviarPoliza").addEventListener("click", funbtnEnviarPoliza);

function funembedDescargar() {
    gtag('event', 'icono_imprime', { 'event_category': 'casacotizador', 'event_label': '' });
}

function funembedEnviarCorreo() {
    gtag('event', 'icono_envia_cotizacion', { 'event_category': 'casacotizador', 'event_label': '' });
}

function funbtnEnviarPoliza() {
    gtag('event', 'icono_imprime', { 'event_category': 'consultapoliza', 'event_label': 'imprimir_enviar' });
}