﻿document.getElementById("divContactar").addEventListener("click", fundivContactar);
document.getElementById("btnEnviarDatos").addEventListener("click", funbtnEnviarDatos);
document.getElementById("btnCancelar").addEventListener("click", funbtnCancelar);

function fundivContactar() {

   gtag('event', 'lead_gen', {'event_category': 'hogar_cotizador','event_label': 'te_llamamos'});
}
function funbtnEnviarDatos() {

  gtag('event', 'nosotros_te_llamamos', { 'event_category': 'cotizador_Casa', 'event_label': 'enviardatos' });
}
function funbtnCancelar() {

  gtag('event', 'nosotros_te_llamamos', { 'event_category': 'cotizador_Casa', 'event_label': 'cancelar' });
}