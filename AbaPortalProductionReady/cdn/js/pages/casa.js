﻿$(function () {
    
});

var periodicidad = "trimestral";

var imprimirPaqueteSeleccionado = function (paqueteId) {
    switch (paqueteId) {
        case "1":
            descripcion = "Basico";
            monto = "$8,315.00 MN";
            montoUnformat = 8315;
            break;
        case "2":
            descripcion = "Amplia";
            monto = "$10,966.00 MN";
            montoUnformat = 10966;
            break;
        case "3":
            descripcion = "Integral";
            monto = "$11,392.00 MN";
            montoUnformat = 11392;
            break;
    }

    switch (periodicidad) {
        case 'mensual':
            formaPago = "Forma de pago: Mensual";
            primerPago = "Primer pago: $2550.00 MN";
            pagoSubsecuente = "11 pagos subsecuentes: $" + parseInt((montoUnformat-2550)/11)  + ".00";
            break;
        case 'trimestral':
            formaPago = "Forma de pago: Trimestral";
            primerPago = "Primer pago: $4550.00 MN";
            pagoSubsecuente = "2 pagos subsecuentes: $" + parseInt((montoUnformat-4550)/2)  + ".00";
            break;
        case 'semestral':
            formaPago = "Forma de pago: Semestral";
            primerPago = "Primer pago: $6550.00 MN";
            pagoSubsecuente = "1 pago subsecuente de: $" + parseInt(montoUnformat-6550) + ".00";
            break;
        case 'anual':
            formaPago = "Forma de pago: Anual";
            primerPago = "Primer pago: $" + monto;
            pagoSubsecuente = "1 único pago de: " + monto;
            break;
    }

    $("[data-Descripcion]").html(descripcion);
    $("[data-Monto]").html(monto);
    $("[data-FormaPago]").html(formaPago);
    $("[data-PrimerPago]").html(primerPago);
    $("[data-PagoSubsecuente]").html(pagoSubsecuente);

    var vigencia = moment().format("L");
    $("[data-Vigencia]").html(vigencia);
    
    $('#pnlSeleccionPaquete .descripcion').html(descripcion);
    $('#pnlSeleccionPaquete .monto').html(monto);
};

$("a[data-action=seleccionarPaquete]").click(function () {
    $("a[data-action=seleccionarPaquete]").parent().removeClass('selected');
    $(this).parent().addClass('selected');

    var paqueteId = $(this).attr("data-paqueteid");
    imprimirPaqueteSeleccionado(paqueteId);
    $('#pnlSeleccionPaquete').removeClass('hidden');

    if ($(window).width() < 1024) {
        if (paqueteId != 2) {
            $('#pnlSeleccionPaquete .opcion-recomendada-header').hide();
            $('#pnlSeleccionPaquete .opcion-recomendada-star').hide();
        }
        else {
            $('#pnlSeleccionPaquete .opcion-recomendada-header').show();
            $('#pnlSeleccionPaquete .opcion-recomendada-star').show();
        }
        
        $('.paquete').addClass('hidden');
        $('#pnlMensualidades .mensualidades').addClass('hidden');
    } 

    return false;
});


$("button[data-action=periodicidad]").click(function () {
    $("button[data-action=periodicidad]").attr("class", "btn btn-bordered");
    $(this).attr("class", "btn btn-bordered-active");

    periodicidad = $(this).attr("data-periodicidad");

    imprimirPaqueteSeleccionado();
});


$("#btnComparaCoberturas").click(function () {
    $('.paquete').addClass('hidden');
    $('#pnlSeleccionPaquete').addClass('hidden'); 
    $('#pnlMensualidades').addClass('hidden');
    $('#tblComparativa').removeClass('hidden');
});

$("#tblComparativa #btnRegresar").click(function () {
    $('.paquete').removeClass('hidden');
    $('#pnlMensualidades').removeClass('hidden');

    if ($(".paquete .itemCotizacion").hasClass('selected'))
    {
        $('#pnlSeleccionPaquete').removeClass('hidden');
    }

    $('#tblComparativa').addClass('hidden');
});


$("#btnCerrar").click(function () {
    $('#pnlSeleccionPaquete').addClass('hidden');
    $(".paquete .itemCotizacion").removeClass('selected')

    $('.paquete').removeClass('hidden');
    $('#pnlMensualidades .mensualidades').removeClass('hidden');
    goToByScroll('seleccionaPaquete');
    //clear selection
});



function goToByScroll(id) {
    // Remove "link" from the ID
    id = id.replace("link", "");
    // Scroll
    $('html,body').animate({
        scrollTop: $("#" + id).offset().top
    },
        'slow');
}

$("a").click(function () {
    idToGo = $(this).attr("href").replace("#", "");
    goToByScroll(idToGo);
});

$("#btnCotizar").click(function () {
    $('#seleccionaPaquete').removeClass('hidden');

    goToByScroll('seleccionaPaquete');
});

$("#btnComprar").click(function () {
    $('#capturarDatos').removeClass('hidden');

    goToByScroll('capturarDatos');
});

$(".capturarDatos #btnAnterior").click(function () {
    goToByScroll('seleccionaPaquete');
});

$("#btnSiguiente").click(function () {
    $('#pnlFormularioPago').removeClass('hidden');

    goToByScroll('pnlFormularioPago');
});

$("#pnlFormularioPago #btnAnterior").click(function () {
    goToByScroll('capturarDatos');
});

$("#btnPagar").click(function () {
    $('#pnlConfirmacionPoliza').removeClass('hidden');

    goToByScroll('pnlConfirmacionPoliza');
});