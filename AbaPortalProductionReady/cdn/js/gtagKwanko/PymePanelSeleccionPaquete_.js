﻿document.getElementById("telefono").addEventListener("click", funtelefono);
document.getElementById("email").addEventListener("click", funemail);
document.getElementById("dudasChat").addEventListener("click", fundudasChat);
document.getElementById("divContactar").addEventListener("click", fundivContactar);
document.getElementById("btnEnviarDatos").addEventListener("click", funbtnEnviarDatos);
document.getElementById("btnCancelar").addEventListener("click", funbtnCancelar);


function funtelefono() {
    gtag('event', 'lead_gen', { 'event_category': 'pyme_cotizador', 'event_label': '1800' });
}
function funemail() {
    gtag('event', 'lead_gen', { 'event_category': 'pyme_cotizador', 'event_label': 'email' });
}
function fundudasChat() {
    gtag('event', 'lead_gen', { 'event_category': 'pyme_cotizador', 'event_label': 'chat' });
}
function fundivContactar() {
    gtag('event', 'lead_gen', { 'event_category': 'pyme_cotizador', 'event_label': 'te_llamamos' });
}
function funbtnEnviarDatos() {
    gtag('event', 'lead_gen', { 'event_category': 'pyme_cotizador', 'event_label': 'te_llamamos_enviar' });
}
function funbtnCancelar() {
    gtag('event', 'lead_gen', { 'event_category': 'pyme_cotizador', 'event_label': 'cancelar' });
}