﻿document.getElementById("autoSeguro").addEventListener("click", funautoSeguro);
document.getElementById("autoCotizadorIndex").addEventListener("click", funautoCotizadorIndex);
document.getElementById("hogarSeguro").addEventListener("click", funhogarSeguro);
document.getElementById("casaCotizadorIndex").addEventListener("click", funcasaCotizadorIndex);
document.getElementById("pymeSeguro").addEventListener("click", funpymeSeguro);
document.getElementById("pymeCotizadorIndex").addEventListener("click", funpymeCotizadorIndex);
document.getElementById("otrosSeguros").addEventListener("click", funotrosSeguros);

function funautoSeguro() {
    gtag('event', 'autoseguro', { 'event_category': 'home', 'event_label': 'ver_mas' });
}
function funautoCotizadorIndex() {
    gtag('event', 'autoseguro', { 'event_category': 'home', 'event_label': 'cotizar' });
}
function funhogarSeguro() {
    gtag('event', 'hogarseguro', { 'event_category': 'home', 'event_label': 'ver_mas' });
}
function funcasaCotizadorIndex() {
    gtag('event', 'hogarseguro', { 'event_category': 'home', 'event_label': 'cotizar' });
}
function funpymeSeguro() {
    gtag('event', 'pymesegura', { 'event_category': 'home', 'event_label': 'ver_mas' });
}
function funpymeCotizadorIndex() {
    gtag('event', 'pymeseguro', { 'event_category': 'home', 'event_label': 'cotizar' });
}
function funotrosSeguros() {
    gtag('event', 'otros_seguros', { 'event_category': 'home', 'event_label': 'ver_mas' });
}