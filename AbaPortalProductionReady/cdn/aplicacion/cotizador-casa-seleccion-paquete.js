﻿
var cotizaciones = [];
var cotizacion = {};
var periodicidad = "mensual";
var cotizacionCargada = false;
var habilitarImpresion = false;
var idpaquetes = [];

var segundaPalabra = function (cadena) {
    var res = cadena.split(" ");
    return res[1];
}

var imprimirPaqueteSeleccionado = function (paqueteId) {
    var paqueteSeleccionado = {};

    $.each(cotizacion.Paquetes, function (index, paquete) {
        if (paquete.PaqueteId + "" === paqueteId + "") {
            var monto = 0;
            var primerPago = 0;
            var cantidadPagos = 0;
            var pagoSubsecuente = 0;

            var imprimirPrimerPago = true;

            switch (periodicidad) {
                case 'mensual':
                    pagoSubsecuente = paquete.ReciboSubsecuente;
                    cantidadPagos = 12;
                    break;
                case 'trimestral':
                    pagoSubsecuente = paquete.ReciboSubsecuente;
                    cantidadPagos = 4;
                    break;
                case 'semestral':
                    pagoSubsecuente = paquete.ReciboSubsecuente;
                    cantidadPagos = 2;
                    break;
                case 'anual':
                    pagoSubsecuente = paquete.PrimerRecibo;
                    cantidadPagos = 1;
                    break;
            }

            paqueteSeleccionado = {
                PaqueteId: paquete.PaqueteId,
                Periodicidad: periodicidad,
                Monto: paquete.PrimaTotalFormato,
                Descripcion: segundaPalabra(paquete.PaqueteNombre),
                Vigencia: moment(paquete.InicioVigencia).format("L"),
                PrimerPago: '$' + (paquete.PrimerRecibo).formatMoney(2, '.', ',') ,
                ImprimirPrimerPago: imprimirPrimerPago,
                CantidadPagos: cantidadPagos,
                PagoSubsecuente: '$' + (pagoSubsecuente).formatMoney(2, '.', ',') 
            };
            $("#leadGenMontoPaquete").val(paquete.PrimaTotalFormato);
            $("#leadGenPaquete").val(paquete.PaqueteNombre);

            var JSONInfoProspecto = {
                'Origen': "CasaCotizador",
                'Producto': "HOGAR SEGURO",
                'PaqueteSeleccionado': $("#leadGenPaquete").val(),
                'FormaPago': $("#leadGenFormaPago").val(),
                'MontoPaquete': $("#leadGenMontoPaquete").val(),
                'PerfilCliente': localStorage.getItem("cbxCasa") == null ? "" : localStorage.getItem("cbxCasa")
            };
            localStorage.setItem('CasaInfoProspecto', JSON.stringify(JSONInfoProspecto));

            $("#pnlSeleccionPaquete").load("/CasaCotizador/PaqueteSeleccionado_", { paquete: paqueteSeleccionado });
            $("#seleccionPaquete").val(paquete.PaqueteId);
            $("#paqueteSeleccionadoId").val(paquete.PaqueteId);
            $("#seleccionPaqueteInformacion").val(paquete.Info);

            window.localStorage.setItem("monto", paquete.PrimerRecibo);
            $('#tipovivienda').unbind('change');
            $('#tipovivienda').change(function () {
                if ($('#tipovivienda').val() != -1) $('#Errortipovivienda').slideUp();
                $('#tipoViviendaId').val($('#tipovivienda').val());
            });
            //console.log("paquete info:" + paquete.Info);
        } //endif
    });
    console.log("Paquete seleccionado Id:" + paqueteId);
};

function number_format(number, decimals, dec_point, thousands_sep) {
    //  discuss at: http://phpjs.org/functions/number_format/
    // original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
    // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: davook
    // improved by: Brett Zamir (http://brett-zamir.me)
    // improved by: Brett Zamir (http://brett-zamir.me)
    // improved by: Theriault
    // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // bugfixed by: Michael White (http://getsprink.com)
    // bugfixed by: Benjamin Lupton
    // bugfixed by: Allan Jensen (http://www.winternet.no)
    // bugfixed by: Howard Yeend
    // bugfixed by: Diogo Resende
    // bugfixed by: Rival
    // bugfixed by: Brett Zamir (http://brett-zamir.me)
    //  revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
    //  revised by: Luke Smith (http://lucassmith.name)
    //    input by: Kheang Hok Chin (http://www.distantia.ca/)
    //    input by: Jay Klehr
    //    input by: Amir Habibi (http://www.residence-mixte.com/)
    //    input by: Amirouche
    //   example 1: number_format(1234.56);
    //   returns 1: '1,235'
    //   example 2: number_format(1234.56, 2, ',', ' ');
    //   returns 2: '1 234,56'
    //   example 3: number_format(1234.5678, 2, '.', '');
    //   returns 3: '1234.57'
    //   example 4: number_format(67, 2, ',', '.');
    //   returns 4: '67,00'
    //   example 5: number_format(1000);
    //   returns 5: '1,000'
    //   example 6: number_format(67.311, 2);
    //   returns 6: '67.31'
    //   example 7: number_format(1000.55, 1);
    //   returns 7: '1,000.6'
    //   example 8: number_format(67000, 5, ',', '.');
    //   returns 8: '67.000,00000'
    //   example 9: number_format(0.9, 0);
    //   returns 9: '1'
    //  example 10: number_format('1.20', 2);
    //  returns 10: '1.20'
    //  example 11: number_format('1.20', 4);
    //  returns 11: '1.2000'
    //  example 12: number_format('1.2000', 3);
    //  returns 12: '1.200'
    //  example 13: number_format('1 000,50', 2, '.', ' ');
    //  returns 13: '100 050.00'
    //  example 14: number_format(1e-8, 8, '.', '');
    //  returns 14: '0.00000001'

    number = (number + '')
      .replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
      prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
      sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
      dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
      s = '',
      toFixedFix = function (n, prec) {
          var k = Math.pow(10, prec);
          return '' + (Math.round(n * k) / k)
            .toFixed(prec);
      };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
      .split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '')
      .length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1)
          .join('0');
    }
    return s.join(dec);
}

var imprimirSelectorPaquetes = function (paquetes) {
    console.log(paquetes)
    var paquetesConFormato = [];
    $("#leadGenFormaPago").val(periodicidad);
    console.log("Periodicidad:" + periodicidad);
    console.log(paquetes);

    //for (var i=0; i<3; i++){
    //    if (i == 0) paquetes[i].videoSrc = "https://www.youtube.com/embed/IlIntNdIC-8?ecver=2";
    //    else if (i == 1) paquetes[i].videoSrc = "https://www.youtube.com/embed/rz4DxhqL7NA?ecver=2";
    //    else if (i == 2) paquetes[i].videoSrc = "https://www.youtube.com/embed/4VDn9lCiP_s?ecver=2";
    //    else paquetes[i].videoSrc = "https://www.youtube.com/embed/R2eMcs_g9cM?ecver=2";
    //}
    var valido = true;
    $.each(paquetes, function (index, paquete) {
               
        paquetesConFormato.push({
            PaqueteId: paquete.PaqueteId,
            PaquetePreferente: paquete.PaquetePreferente,
            Descripcion: segundaPalabra(paquete.PaqueteNombre),
            Monto: '$' + number_format(periodicidad == "anual" ? paquete.PrimerRecibo: paquete.ReciboSubsecuente, 2, '.', ',') + " " + periodicidad + "es",
            ClasePreferente: paquete.PaquetePreferente === true ? " opcion-recomendada" : "",
            VideoSrc: paquete.videoSrc
        });
    });
   
    $("#seleccionCoberturasStack").load("/CasaCotizador/ListadoPaquetes_", { paquetes: paquetesConFormato });
    imprimirPaqueteSeleccionado(cotizacion.PaqueteSeleccionadoId);
    
};

var initialize = function () {
    cargarPaquetes(27);
};

var cargarPaquetes = function (TipoPagoId) {
    $('#CotizacionTipoPagoId').val(TipoPagoId);
    $.ajax({
        url: apiRoot + "CasaCotizador/ObtenerPaquetes",
        method: 'POST',
        data: $("#frmDatosCotizacionCasa").serialize(),
        beforeSend: function () {
            $('#Lightbox').show();
        },
        complete: function () {
            $('#Lightbox').hide();
        },
        success: function (response) {
            var valido = true;
            var loadComparativa = true;
            var esMascota = 0;
            $.each(response, function (index, itemcotizacion) {
                if (itemcotizacion.Paquetes.length == 0) valido = false;
                if (idpaquetes.length > 0) loadComparativa = false;
                $.each(itemcotizacion.Paquetes, function (index, paquete) {
                    if (esMascota == 0) {
                        var urlBanner = "";
                        esMascota = paquete.PaqueteNombre.toUpperCase().search("MASCOTA");
                        if (esMascota != -1) urlBanner = $('[name=urlMascota]').val();
                        else urlBanner = $('[name=urlTecnico]').val();
                        $('#bannerPaquetesCasa').attr('src', urlBanner);
                    }

                    if(loadComparativa) idpaquetes.push({ PaqueteId: paquete.PaqueteId, Descripcion: paquete.PaqueteNombre, PaquetePreferente: paquete.PaquetePreferente });
                    if (paquete.PrimaTotal == 0)
                        valido = false;                       
                });
                if(valido)  cotizaciones.push(itemcotizacion);
            });
            if (valido) {
                $.each(cotizaciones, function (index, itemcotizacion) {
                    cotizacion = itemcotizacion;
                });
                imprimirSelectorPaquetes(cotizacion.Paquetes);
                imprimirPaqueteSeleccionado(cotizacion.PaqueteSeleccionadoId);
                if(loadComparativa) consultarCoberturas(idpaquetes);
                habilitarImpresion = true;
            } else {
                $('#datosPaqueteContainer').html('');
                $("#mensajeErrorPaquetes").show();
            }
        }
    });

}

var modoImpresionPoliza = "impresion";

var inicializarComandosImpresionCotizacion = function () {
    function base64ToArrayBuffer(base64) {
        var binaryString = window.atob(base64);
        var binaryLen = binaryString.length;
        var bytes = new Uint8Array(binaryLen);
        for (var i = 0; i < binaryLen; i++) {
            var ascii = binaryString.charCodeAt(i);
            bytes[i] = ascii;
        }
        return bytes;
    }

    var validarFormularioImpresion = function () {
        var isValid = true;

        if (!($("#txtNombreCotizacion").val().length > 0)) {
            isValid = false;
            $("#containerNombreCotizacion").addClass("has-warning");
            $("#lblErrorNombre").show();
        }

        if (modoImpresionPoliza === "correo") {
            if (!($("#txtCotizacionCorreoElectronico").val().length > 0)) {
                isValid = false;
                $("#containerCorreoCotizacion").addClass("has-warning");
                $("#lblErrorCorreo").html('<small>Es necesario que indique su correo electr&oacute;nico</small>')
                $("#lblErrorCorreo").show();
            }
            else {
                if (!utils.IsValidEmailAddress($("#txtCotizacionCorreoElectronico").val())) {
                    $("#containerCorreoCotizacion").addClass("has-warning");
                    $("#lblErrorCorreo").html('<small>Direcci&oacute;n de correo inv&aacute;lida. Por favor verifique.</small>')
                    $("#lblErrorCorreo").show();
                    isValid = false;
                }

                if (utils.IsValidEmailAddress($("#txtCotizacionConfirmaCorreoElectronico").val())) {
                    var correo = $("#txtCotizacionCorreoElectronico").val();
                    var confirmacion = $("#txtCotizacionConfirmaCorreoElectronico").val();

                    if (correo !== confirmacion) {
                        $("#containerConfirmaCorreoCotizacion").addClass("has-warning");
                        $("#lblErrorConfirmarCorreo").html('<small>Valide su direcci&oacute;n de correo el&eacute;ctronico..</small>')
                        $("#lblErrorConfirmarCorreo").show();
                        isValid = false;
                    }
                }
                else
                {
                    $("#containerConfirmaCorreoCotizacion").addClass("has-warning");
                    $("#lblErrorConfirmarCorreo").html('<small>Direcci&oacute;n de correo inv&aacute;lida. Por favor verifique.</small>')
                    $("#lblErrorConfirmarCorreo").show();
                    isValid = false;
                }


            }
        }

        return isValid;
    }

    $("#txtNombreCotizacion").focus(function () {
        $(this).parent().removeClass("has-warning");
        $("#lblErrorNombre").hide();
    });

    $("#txtCotizacionCorreoElectronico").focus(function () {
        $(this).parent().removeClass("has-warning");
        $("#lblErrorCorreo").hide();
    });

    $("#txtCotizacionConfirmaCorreoElectronico").focus(function () {
        $(this).parent().removeClass("has-warning");
        $("#lblErrorConfirmarCorreo").hide();
    });

    $("#lnkImprimirCotizacion").click(function () {
        if (habilitarImpresion === true) {
            $("#btnImprimirCotizacion").removeAttr("disabled");
            modoImpresionPoliza = "impresion";
            $("#pnlImprimirCotizacionCorreoEl").hide();
            $('#pnlImprimirCotizacionModal').modal({
                keyboard: true
            });

            $('#lblNombre').html('A nombre de quién quieres imprimir tu cotización: ');

            $("#pnlImprimirCotizacionModal").find(".modal-title").html("Imprimir cotización");
            $("#btnImprimirCotizacion").html("Imprimir cotización");
            $("#btnImprimirCotizacion").attr("onclick", "gtag('event', 'icono_imprime', {'event_category': 'hogar_cotizador','event_label': 'imprimir_cotizacion'});");
        }
        else {
            //alert("Información de paquetes no disponible aún.  Por favor espere.")
        }
        return false;
    });

    $("#chkAceptarImpresion").change(function () {
        if ($('#chkAceptarImpresion').is(':checked')) {
            $("#btnImprimirCotizacion").removeAttr("disabled");
        }
        else {
            $("#btnImprimirCotizacion").attr("disabled", "disabled");
        }
    });

    $("#lnkImprimirCotizacionCorreo").click(function () {
        if (habilitarImpresion === true) {
            modoImpresionPoliza = "correo";
            $("#chkAceptarImpresion").removeAttr("checked");
            $("#btnImprimirCotizacion").attr("disabled", "disabled");
            $("#pnlImprimirCotizacionCorreoEl").show();
            $("#txtCotizacionCorreoElectronico").val("");
            $('#txtCotizacionConfirmaCorreoElectronico').val("");
            $('#pnlImprimirCotizacionModal').modal({
                keyboard: true
            });

            $('#lblNombre').html('A nombre de quién enviamos tu cotización: ');
            $("#pnlImprimirCotizacionModal").find(".modal-title").html("Enviar cotización");
            $("#btnImprimirCotizacion").html("Enviar cotización");
            $("#btnImprimirCotizacion").attr("onclick", "gtag('event', 'icono_envia_cotizacion', {'event_category': 'hogar_cotizador','event_label': 'enviar_cotizacion'});");
        }
        else {
            alert("Información de paquetes no disponible aún.  Porfavor espere.")
        }

        return false;
    });

    $("#btnImprimirCotizacion").click(function () {
        var nombre = $("#txtNombreCotizacion").val();
        var correoElectronico = $("#txtCotizacionCorreoElectronico").val();
        $("#nombreUsuario").val(nombre);
       if (validarFormularioImpresion()) {
            var html = $(this).html();

            if (html === 'Enviar cotización') {
                $("#CorreoElectronicoUsuario").val(correoElectronico);
                $.post(apiRoot + "CasaCotizador/EnviarCotizacion", $("#frmFormularioComprar").serialize(), function (res) {
                    $("#btnImprimirCotizacion").removeAttr("disabled");
                    $('#pnlImprimirCotizacionModal').modal('hide');
                    $('#containerPrint').hide();
                }).fail(function () {
                    $('#containerPrint').hide();
                });
            }
            else {
                $.ajax({
                    url: apiRoot + "CasaCotizador/GenerarCotizacion",
                    method: 'POST',
                    data: $("#frmFormularioComprar").serialize(),
                    dataType: 'binary',
                    processData: false,
                    beforeSend: function (xhr, settings) {
                        $('#containerPrint').show();
                        $("#btnImprimirCotizacion").attr("disabled", "disabled");
                        $("#nombreUsuario").val($("#txtNombreCotizacion").val());
                        $("#CorreoElectronicoUsuario").val($("#txtCotizacionCorreoElectronico").val());
                        //$("#periodicidad").val(periodicidad);
                    },
                    complete: function (xhr, textStatus) {
                        $('#containerPrint').hide();
                    },
                    error: function (xhr, textStatus, data) {
                        console.error(textStatus);
                        $('#containerPrint').hide();
                    },
                    success: function (response, status, xhr) {
                        $("#btnImprimirCotizacion").removeAttr("disabled");
                        $('#pnlImprimirCotizacionModal').modal('hide');
                        var filename = "";
                        var disposition = xhr.getResponseHeader("Content-Disposition");

                        if (disposition && disposition.indexOf("attachment") !== -1) {
                            var filenameRegex = /filename[^;=\n]*=(([""]).*?\2|[^;\n]*)/;
                            var matches = filenameRegex.exec(disposition);
                            if (matches !== null && matches[1]) {
                                filename = matches[1].replace(/[""]/g, "");
                            }
                        }

                        var type = xhr.getResponseHeader("Content-Type");
                        var data = new Blob([response], { type: type });

                        if (typeof window.navigator.msSaveBlob !== 'undefined') {
                            window.navigator.msSaveBlob(data, filename);
                        }
                        else {
                            var URL = window.URL || window.webkitURL;
                            var downloadURL = URL.createObjectURL(data);

                            if (filename) {
                                var a = document.createElement('a');

                                if (typeof a.download == 'undefined') {
                                    window.location = downloadURL;
                                }
                                else {
                                    a.href = downloadURL;
                                    a.download = filename;
                                    document.body.appendChild(a);
                                    a.click();
                                }
                            }
                            else {
                                window.location = downloadURL;
                            }

                            setTimeout(function () {
                                URL.revokeObjectURL(downloadURL);
                            }, 100);
                        }

                        $('#containerPrint').hide();
                    }
                });
            }
        }
        return false;
    });

    $('#divContactar').click(function () {
        $('#modalLlamamos').find('.has-warning').removeClass("has-warning");
        $('#modalLlamamos').find('.alert-warning').slideUp();
        $('#modalLlamamos').modal('show');
    });

    $('#btnEnviarDatos').click(function () {
        teLlamamos();
    });

    $('#Telefono, #TelefonoCasa').on('keydown', function (e) {
        //var keyCode = ('which' in event) ? event.which : event.keyCode;

        //isNotWanted = (keyCode == 69 || keyCode == 101);
        //return !isNotWanted;
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || (e.keyCode >= 35 && e.keyCode <= 40)) {
            return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
};

var teLlamamos = function () {
    var isValid = true;
    if (!($("#txtNombreContactar").val().length > 0)) {
        isValid = false;
        $("#containerNombreCotizacion").addClass("has-warning");
        $("#lblErrorNombreContactar").slideDown();
    }
    if (!($("#txtTelefonoContactar").val().length > 0)) {
        isValid = false;
        $("#txtTelefonoContactar").addClass("has-warning");
        $("#lblErrorTelefonoContactar").slideDown();
    } else if (!($("#txtTelefonoContactar").val().length == 10)) {

        isValid = false;
        $("#txtTelefonoContactar").addClass("has-warning");
        $("#lblErrorTelefonoContactar").slideDown();
    }

    if (!($("#txtCorreoElectronicoContactar").val().length > 0)) {
        isValid = false;
        $("#txtCorreoElectronicoContactar").addClass("has-warning");
        $("#lblErrorCorreoContactar").html('<small>Es necesario que indique su correo electr&oacute;nico</small>')
        $("#lblErrorCorreoContactar").show();
    }
    else {
        if (!utils.IsValidEmailAddress($("#txtCorreoElectronicoContactar").val())) {
            $("#txtCorreoElectronicoContactar").addClass("has-warning");
            $("#lblErrorCorreoContactar").html('<small>Direcci&oacute;n de correo inv&aacute;lida. Por favor verifique.</small>')
            $("#lblErrorCorreoContactar").show();
            isValid = false;
        }
    }
    if (($("#txtTelefonoFijo").val().length > 0)) {
        if (!($("#txtTelefonoFijo").val().length == 10)) {

            isValid = false;
            $("#txtTelefonoFijo").addClass("has-warning");
            $("#lblErrorTelefonoFijo").slideDown();
        }
    } else {
        $("#lblErrorTelefonoFijo").slideUp();
    }

    if (isValid) {
        var paquete = $("#leadGenPaquete").val();
        var formaPago = $("#leadGenFormaPago").val();
        var montoPaquete = $("#leadGenMontoPaquete").val();
        var perfilCliente = localStorage.getItem("cbxCasa") == null ? "" : localStorage.getItem("cbxCasa");

        $.ajax({
            url: webSiteRoot + "ServiciosExpress/LlamadaLeadGen",
            method: 'POST',
            data: {
                NombreCompleto: $("#txtNombreContactar").val(),
                Email: $("#txtCorreoElectronicoContactar").val(),
                TelefonoCompletoCelular: $("#txtTelefonoContactar").val(),
                TelefonoCompletoCasa: $("#txtTelefonoFijo").val() == null ? "" : $("#txtTelefonoFijo").val(),
                Origen: "CasaCotizador",
                Producto: "HOGAR SEGURO",
                PaqueteSeleccionado: paquete,
                FormaPago: formaPago,
                MontoPaquete: montoPaquete,
                PerfilCliente: perfilCliente
            },
            beforeSend: function () {
                $('#Lightbox').find('#spinnerLight').contents()[2].nodeValue = 'Espere un momento ...';
                $('#Lightbox').show();
                $('#lblLeadGenErrorProceso').hide();
            },
            complete: function () {
                $('#Lightbox').hide();
            },
            success: function (response) {
                if (response.Response) {
                    $('#modalLlamamos').modal('hide');
                } else {
                    $('#lblLeadGenErrorProceso').show();
                }
            }
        });
    }
};

var consultarCoberturas = function (idPaquetes) {

    $.ajax({
        url: webSiteRoot + "CasaCotizador/ObtenerCoberturas",
        method: 'POST',
        data: {
            Paquetes: idPaquetes
        },
        beforeSend: function () {
            $('#Lightbox').find('#spinnerLight').contents()[2].nodeValue = 'Espere un momento ...';
            $('#Lightbox').show();
        },
        complete: function () {
            $('#Lightbox').hide();
        },
        success: function (response) {
            $('#panelTablaComparativa').html(response);
        }
    });
}

$(function () {
    $("#tipoPagoId").val("27"); //iniciar con periodicidad 'Mensual'

    $("#btnMostrarComparativas,#btnMostrarTodasComparativas").click(function () {
        $("#panelSelectorCotizador").hide();
        $("#panelComparativas").show();
        $("#selectorMensualidades").hide();

        location.href = '#encabezadoCotizador';
    });

    $("#btnVolverASelectorPlanes").click(function () {
        $("#panelComparativas").hide();
        $("#selectorMensualidades").show();
        $("#panelSelectorCotizador").show();
        location.href = '#encabezadoCotizador';
    });

    $(document).on("click", "#btnComprar", function () {
        var isValid = true;
        if ($('#tipovivienda').val() == -1) isValid = false;
        $('#Lightbox').show();
        $('#Lightbox').find('#spinnerLight').contents()[2].nodeValue = 'Espere un momento ...';
        $("input[data-behavior=mustbechecked]").each(function () {
            if (!$(this).is(":checked")) {
                isValid = false;
            }
        });

        if (!isValid) {
            $("#pnlErrorPago").slideDown();
            $('#Lightbox').hide();
        } else {
            $("#periodicidad").val(periodicidad);
            $("#frmFormularioComprar").submit();
        }
    });

    $("button[data-action=periodicidad]").click(function () {

        //Aplicar estilo al boton activo:
        $("button[data-action=periodicidad]").attr("class", "btn btn-bordered");
        $(this).attr("class", "btn btn-bordered-active");

        periodicidad = $(this).attr("data-periodicidad");
        $('#periodicidad').val($(this).attr('data-periodicidad'));
        $("#periodicidadId").val($(this).attr("data-periodicidad-id"));
        $("#tipoPagoId").val($(this).attr("data-periodicidad-id"));
        console.log("PeriodicidadId:" + $("#periodicidadId").val());
        $.each(cotizaciones, function (index, itemcotizacion) {
            if (itemcotizacion.TipoPagoId == $("#periodicidadId").val()) {
                cotizacionCargada = true;
                cotizacion = itemcotizacion;
                return false;
            } else {
                cotizacionCargada = false;
            }
        });
        if (cotizacionCargada == true) {
            imprimirSelectorPaquetes(cotizacion.Paquetes);
        } else {
            cargarPaquetes($("#periodicidadId").val());
        }
    });

    $(document).on("click", "a[data-action=seleccionarPaquete]", function () {
        $("a[data-action=seleccionarPaquete]").parent().removeClass('opcion-recomendada');
        $(this).parent().addClass('opcion-recomendada');

        goToByScroll("pnlSeleccionPaquete");

        var paqueteId = $(this).attr("data-paqueteid");

        if (paqueteId == 1) {
            $(this).parent().parent().find('.opcion-recomendada-header').removeClass('notselected')
        } else {
            $(this).parent().parent().find('.opcion-recomendada-header').addClass('notselected')
        }

        console.log("Paquete seleccionado:" + paqueteId);
        imprimirPaqueteSeleccionado(paqueteId);
        return false;
    });

    function goToByScroll(id) {
        // Remove "link" from the ID
        id = id.replace("link", "");
        // Scroll
        $('html,body').animate({
            scrollTop: $("#" + id).offset().top -110
        },
            'slow');
    }

    //$("a").click(function () {
    //    idToGo = $(this).attr("href").replace("#", "");
    //    goToByScroll(idToGo);
    //});

    initialize();
    inicializarComandosImpresionCotizacion();
    $('#periodicidad').val('mensual');

    $('#btnComprarPaqueteCasa').click(function () {
        $('#frmFormularioComprar').submit();
    });

    $('#dudasChat').click(function () {
        $("#lnkLanzarChat").click();
        $('#chatHeader').click();
    });

    $('#txtNombreCotizacion, #txtNombreContactar').keypress(function (e) {
        var regex = /^[a-zA-Z\s]+$/
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        else {
            e.preventDefault();
            return false;
        }
    });

    $('#txtTelefonoContactar, #txtTelefonoFijo').on('keydown', function (e) {
        //var keyCode = ('which' in event) ? event.which : event.keyCode;

        //isNotWanted = (keyCode == 69 || keyCode == 101);
        //return !isNotWanted;
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || (e.keyCode >= 35 && e.keyCode <= 40)) {
            return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});