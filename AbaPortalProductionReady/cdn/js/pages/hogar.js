﻿// Disabled Icons
disabledIcons = function () {
    $('#icon-1').addClass('icon-disabled');
    $('#icon-2').addClass('icon-disabled');
    $('#icon-3').addClass('icon-disabled');
    $('#icon-4').addClass('icon-disabled');
    $('#icon-5').addClass('icon-disabled');
    $('#icon-6').addClass('icon-disabled');
    $('#icon-7').addClass('icon-disabled');
    $('#icon-8').addClass('icon-disabled');
    $('#icon-9').addClass('icon-disabled');
    $('#icon-10').addClass('icon-disabled');
    $('#icon-11').addClass('icon-disabled');
    $('#icon-12').addClass('icon-disabled');
    $('#icon-13').addClass('icon-disabled');
};


EnablePackages = function (packageType) {
    disabledIcons();
    $('#icon-1').removeClass('icon-disabled');
    $('#icon-2').removeClass('icon-disabled');
    $('#icon-3').removeClass('icon-disabled');
    $('#icon-4').removeClass('icon-disabled');
    $('#icon-5').removeClass('icon-disabled');
    $('#icon-6').addClass('icon-disabled');
    $('#icon-7').addClass('icon-disabled');
    $('#icon-8').addClass('icon-disabled');
    $('#icon-9').removeClass('icon-disabled');
    $('#icon-10').removeClass('icon-disabled');
    $('#icon-11').removeClass('icon-disabled');
    $('#icon-12').removeClass('icon-disabled');
    $('#icon-13').removeClass('icon-disabled');

    if (packageType == 1) {
        $('#icon-1').removeClass('icon-disabled');
        $('#icon-2').removeClass('icon-disabled');
        $('#icon-3').removeClass('icon-disabled');
        $('#icon-4').removeClass('icon-disabled');
        $('#icon-5').addClass('icon-disabled');
        $('#icon-6').addClass('icon-disabled');
        $('#icon-7').addClass('icon-disabled');
        $('#icon-8').addClass('icon-disabled');
        $('#icon-9').removeClass('icon-disabled');
        $('#icon-10').removeClass('icon-disabled');
        $('#icon-11').removeClass('icon-disabled');
        $('#icon-12').removeClass('icon-disabled');
        $('#icon-13').removeClass('icon-disabled');
    } else if (packageType == 2) {
        $('#icon-1').removeClass('icon-disabled');
        $('#icon-2').removeClass('icon-disabled');
        $('#icon-3').removeClass('icon-disabled');
        $('#icon-4').removeClass('icon-disabled');
        $('#icon-5').removeClass('icon-disabled');
        $('#icon-6').addClass('icon-disabled');
        $('#icon-7').addClass('icon-disabled');
        $('#icon-8').addClass('icon-disabled');
        $('#icon-9').removeClass('icon-disabled');
        $('#icon-10').removeClass('icon-disabled');
        $('#icon-11').removeClass('icon-disabled');
        $('#icon-12').removeClass('icon-disabled');
        $('#icon-13').removeClass('icon-disabled');
    } else if (packageType == 3) {
        $('#icon-1').removeClass('icon-disabled');
        $('#icon-2').removeClass('icon-disabled');
        $('#icon-3').removeClass('icon-disabled');
        $('#icon-4').removeClass('icon-disabled');
        $('#icon-5').removeClass('icon-disabled');
        $('#icon-6').removeClass('icon-disabled');
        $('#icon-7').removeClass('icon-disabled');
        $('#icon-8').removeClass('icon-disabled');
        $('#icon-9').removeClass('icon-disabled');
        $('#icon-10').removeClass('icon-disabled');
        $('#icon-11').removeClass('icon-disabled');
        $('#icon-12').removeClass('icon-disabled');
        $('#icon-13').removeClass('icon-disabled');
    }
}

// Gray Floating Window
$('.section-cotizador-auto-flotante .close').click(function () {
    $(this).closest('.section-cotizador-auto-flotante').hide();
});

// Tipes of benefits
$('.auto-servicios').click(function () {
    $('.auto-servicios').removeClass('estelar');
    $('.auto-servicios').find('.title').css({ 'color': 'black', 'font-weight': '300' });
    $('.auto-servicios').find('span').css('color', '#7A7B7D');
    $('.auto-servicios').find('.circle-inactive').css('color', '#7A7B7D');
    $(this).addClass('estelar');

    $(this).find('.title').css({ 'color': 'white', 'font-weight': 'bold' });
    $(this).find('span').css('color', 'white').css('opacity', '0.5');
    $(this).find('.circle-inactive').css('color', 'white').css('opacity', '0.5');
});

// Icons
$('#limitada, .limitada').click(function () {
    EnablePackages(1);
});

$('#amplia, .amplia').click(function () {
    EnablePackages(2);
});

$('#integral, .integral').click(function () {
    EnablePackages(3);
});

$('#phonePackages .auto-servicios').click(function () {
    id = "iconos";

    $('html,body').animate({
        scrollTop: $("#" + id).offset().top - 105
    }, 'slow');

});

$('#ver-tabla').click(function () {
    $('.ocultar').fadeOut('slow');
    $('.tbl-amortizacion').fadeIn('slow');
    $('#phonePackages').hide();
    $('.auto-servicios').hide();
    $(this).hide();
    $('#ocultar-tabla').show();
    $('html,body').animate({
        scrollTop: $(".tbl-amortizacion").offset().top - 140
    }, 1000);
});

$('#ocultar-tabla').click(function () {
    $('.tbl-amortizacion').fadeOut('slow');
    $('.ocultar').fadeIn('slow');
    $('#phonePackages').show();
    $('.auto-servicios').show();
    $(this).hide();
    $('#ver-tabla').show();
    $('html,body').animate({
        scrollTop: $(".auto-servicios").offset().top - 140
    }, 1000);
});

// Hide/Show Detail of Benefits in Table
$('.tbl-amortizacion tbody> tr > td > i').click(function (e) {
    e.preventDefault();

    // Get the Parent
    var data = $(this).data('benefit');

    if (data !== undefined) {
        var row_index = $(this).parent().parent().index();
        var parent = $('.tbl-amortizacion > tbody > tr:eq(' + row_index + ')');
        var benefitMessage = parent.data('benefit-message');

        if (benefitMessage !== undefined) {
            var html = '<tr id="' + data + '"><td colspan="9">' + benefitMessage + '</td></tr>';

            if ($('#' + data).length > 0) {
                $(this).removeClass('fa-sort-asc').addClass('fa-sort-desc');
                $('#' + data).remove();
            } else {
                parent.after(html);
                $(this).removeClass('fa-sort-desc').addClass('fa-sort-asc');
            }
        }
    }
});

/**
 * Submenu animation function
 */
$('a[href*="#"]:not([href="#"])').click(function () {
    if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
                scrollTop: target.offset().top - 100
            }, 1000);
            return false;
        }
    }
});

/**
 * Active/Deactive in submenu
 */
$('.submenu-item').click(function () {
    $('.submenu-item').removeClass('active');
    $(this).addClass('active');
});

// First option in packages selected
$('#amplia, .amplia').addClass('estelar').find('.title').css({ 'color': 'white', 'font-weight': 'bold' }).find('span').css('color', 'white').css('opacity', '0.5').find('.circle-inactive').css('color', 'white').css('opacity', '0.5');
EnablePackages(2);