﻿///<reference path="~/cdn/js/underscore-min.js"/>

var imprimirResumenErrores = function (errores) {
    $("#pnlErrorCaptura").load("/Contenidos/ListadoErrores_", { errores: errores });
    $("#pnlErrorCaptura").show();
};

var validaEmail = function (email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

var validarFormulario = function () {
    var errores = [];
    var esValido = true;
    if ($('#Telefono').val() === '' /*&& $('#TelefonoCasa').val() === ''*/) {
        $('#Telefono').parent().attr("class", "control-container has-error");
        //$('#TelefonoCasa').parent().attr("class", "control-container has-error");

        errores.push({
            detalle: 'El campo [' + $('#Telefono').attr("placeholder") + '] es requerido.'/* +' o [' + $('#TelefonoCasa').attr("placeholder") + '] se requieren alguno de los dos.'*/
        });

        esValido = false;
    }

    if (!validaEmail($(".email").val())) {
        $(".email").parent().attr("class", "control-container has-error");
        errores.push({
            detalle: 'El campo [' + $(".email").attr("placeholder") + '] no tiene un email válido.'
        });
        esValido = false;
    }

    if (!validaEmail($(".emailconfirm").val())) {
        $(".emailconfirm").parent().attr("class", "control-container has-error");
        errores.push({
            detalle: 'El campo [' + $(".emailconfirm").attr("placeholder") + '] no tiene un email válido.'
        });
        esValido = false;
    }

    if ($(".email").val() != $(".emailconfirm").val()) {
        errores.push({
            detalle: 'Los campos [' + $(".email").attr("placeholder") + '] y [' + $(".emailconfirm").attr("placeholder") + '] deben de ser iguales.'
        });
        esValido = false;
    }

    $("input[required=required]").each(function () {
        if ($(this).val() === '') {
            $(this).parent().attr("class", "control-container has-error");

            errores.push({
                detalle: 'El campo [' + $(this).attr("placeholder") + '] es requerido.'
            });

            esValido = false;
        }
    });

    $("select[required=required]").each(function () {
        if ($(this).val() === '-1') {
            $(this).parent().attr("class", "control-container has-error");

            errores.push({
                detalle: $(this).attr("placeholder")
            });

            esValido = false;
        }
    });

    if (esValido) {
        var dia = $("#Dia").val();
        var mes = $("#Mes").val();
        var año = $("#Anio").val();

        var fechaNacimiento = moment(año+mes+dia);

        if (!fechaNacimiento.isValid()) {
            errores.push({
                detalle: "La fecha de nacimiento es inválida."
            });
            esValido = false;
        }
        else {
            var edad = moment().diff(fechaNacimiento, 'years');
            if (edad < 18) {
                errores.push({
                    detalle: "Las personas menores de 18 años no pueden contratar la políza."
                });
                esValido = false;
            }
        }
    }

    if (esValido !== true) {
        imprimirResumenErrores(errores);
    }

    return esValido;
};

$(function () {
    //$("#FechaNacimiento").datetimepicker({
    //    format: 'DD/MM/YYYY',
    //    widgetPositioning: { horizontal: 'auto', vertical: 'bottom' },
    //    locale: 'es'
    //});
    $('#vehiculoCotizado').html(sessionStorage.getItem('autoCotizado'));

    $("#btnAnterior").click(function () {
        history.back();
    });

    $("input[required=required], #Telefono, #TelefonoCasa").focus(function () {
        $(this).parent().attr("class", "control-container");
    });

    $("select[required=required]").focus(function () {
        $(this).parent().attr("class", "control-container");
    });

    $("#btnSiguiente").click(function () {
        $("#btnSiguiente").attr("disabled", "disabled");
        $('#Lightbox').show();
        $('#Lightbox').find('#spinnerLight').contents()[2].nodeValue = 'Espere un momento ...';
        if (validarFormulario()) {
            var $form = $('#frmCapturaDatosPersonales');
            var disabled = $form.find(':input:disabled').removeAttr('disabled');
            var serialized = $form.serialize();

            $.post($form.attr("action"), $form.serialize(), function (response) {
                disabled.attr('disabled', 'disabled');
                $('#Lightbox').hide();
                $("#spinDatosProcesando").hide();
                $("#btnSiguiente").removeAttr("disabled");
                if (response.OperacionExitosa === true) {
                    $("#DatosSolicitud").val(response.DatosSolicitud);
                    $("#frmResultadoSolicitud").submit();
                } else {
                    var errores = [];

                    $.each(response.ResultadosValidacion, function (index, error) {
                        errores.push({ detalle: error.Resumen });
                    });

                    imprimirResumenErrores(errores);
                }
            });
        }
        else {
            $("#btnSiguiente").removeAttr("disabled");
            $('#Lightbox').hide();
            $("#spinDatosProcesando").hide();
        }
    });

    $('#Telefono, #TelefonoCasa, #CP').on('keydown', function (e) {
        //var keyCode = ('which' in event) ? event.which : event.keyCode;

        //isNotWanted = (keyCode == 69 || keyCode == 101);
        //return !isNotWanted;
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || (e.keyCode >= 35 && e.keyCode <= 40)) {
            return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $("#CP").autocomplete({
        source: function (request, response) {
            $.getJSON(apiRoot + "Kwanko/ObtenerLocalizacion", {
                cp: request.term,
            }, function (responseData) {
                response(responseData.Localizaciones);
            });
        },
        position: { collision: 'flip' },
        minLength: 5,
        select: function (event, ui) {
            $.getJSON(apiRoot + "Kwanko/ObtenerEstadoMunicipio", {
                Estado: ui.item.Estado,
                Municipio: ui.item.Municipio,
                Poblacion: ui.item.Poblacion
            }, function (responseData) {
                $('#Colonia').val(ui.item.Colonia);
                $('#CP').val(ui.item.CodigoPostal);
                $('#Estado').val(responseData.Estado.Nombre);
                $('#Municipio').val(responseData.Municipio.Nombre);
                $('#Poblacion').val(responseData.Poblacion.Nombre);

                $('#Direccion').focus();
             });

            return false;
        },
        open: function () {
            $(this).autocomplete('widget').css('z-index', 10000);
            return true;
        }
    }).autocomplete('instance')._renderItem = function (ul, item) {
        return $('<li>').append('<div>' + item.Colonia + '</div>').appendTo(ul);
    };

    $('#Genero').val("-1");
    
    $("#Anio").on('change', function () {
        var $frm = $('#frmRfcHide');
        var nombre = $("#Nombre").val();
        var paterno = $("#ApellidoPaterno").val();
        var materno = $("#ApellidoMaterno").val();
        var dia = $("#Dia").val();
        var mes = $("#Mes").val();
        var anio = $("#Anio").val();

        $("#nombre_h").val(nombre);
        $("#paterno_h").val(paterno);
        $("#materno_h").val(materno);
        $("#dia_h").val(dia);
        $("#mes_h").val(mes);
        $("#anio_h").val(anio);

        $.post($frm.attr("action"), $frm.serialize(), function (response) {
            $("#RFC").val(response);
        }, 'json');

    });

    $('#Nombre, #SegundoNombre, #ApellidoPaterno, #ApellidoMaterno').keypress(function (e) {
        var regex = /^[a-zA-Z\s]+$/
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        else {
            e.preventDefault();
            return false;
        }
    });
});