﻿$('#payFirstStep').click(function (e) {
    $('#Lightbox').show();
    $('#Lightbox').find('#spinnerLight').contents()[2].nodeValue = 'Espere un momento ...';
    var poliza = $('#NumPoliza').val();
    var asegurado = $('#Asegurado').val();
    var inciso = $('#Inciso').val();

    if (poliza === '') {
        $('#lblErrorNumSiniestro').show();
        $('#Lightbox').hide();
    }
    else if (asegurado === '') {
        $('#lblErrorAsegurado').show();
        $('#Lightbox').hide();
    }
    else if (inciso === '') {
        $('#lblErrorInciso').show();
        $('#Lightbox').hide();
    } else {
        //var encoded = encodeURI(window.location.href);
        //var uri = 'https://web5.abaseguros.com/acceso/AbrirServicioExpress.aspx?s='+ encoded +'&p=1&m=2&whr=214&np=' + poliza + '&a=' + clave;

        //window.location.replace(uri);
        $('#pagoPolizaForm').submit();
        //var $proximamente = $('#lblProximamente');

        //if (!$proximamente.is(':visible')) {
        //    $('#lblProximamente').show();
        //}
    }
});

$('#NumPoliza').focus(function (e) {
    $('#lblErrorNumSiniestro').hide();
});

$('#Asegurado').focus(function (e) {
    $('#lblErrorAsegurado').hide();
});

$('#Inciso').focus(function (e) {
    $('#lblErrorInciso').hide();
});

var error = utils.GetQueryStringParams('e');

if (error !== undefined) {
    $('#lblErrores .alert-warning > small').html(error);
    $('#lblErrores').show();
}