﻿///<reference path="~/cdn/js/jquery-ui.min.js" />

var presentarMensajeError = function (error) {
    $("#lblError").html(error);
    $("#pnlErrorCotizador").show();
}

var esFormularioValido = function () {
    var esValido = true;

    if ($("#ddlAnio").val() === "0") {
        esValido = false;
    }
    if ($("#txtModelo").val() === "") {
        $("#modeloSeleccionado").val("");
    }
    if ($("#modeloSeleccionado").val() === '') {
        esValido = false;
    }
    if ($("#txtMunicipio").val() === "") {
        $("#municipioSeleccionado").val("");
    }
    if ($("#municipioSeleccionado").val() === '') {
        esValido = false;
    }

    //if (!$('#IsAutoParticularCheckbox').is(':checked')) {
      //  esValido = false;
    //}

    return esValido;
};

var enviarDatosFormulario = function () {
    if (esFormularioValido()) {
        $('#txtModelo').removeAttr('disabled');
        var autoCotizado = $("#txtModelo").val();
        sessionStorage.setItem('autoCotizado', autoCotizado);
        $("#frmCotizador").submit();
    } else {
        presentarMensajeError("Es necesario indicar el a&ntilde;o, el modelo, el municipio y aceptar que su autom&oacute;vil es particular.");
    }
};

var inicializarCotizador = function () {
    $("#txtModelo").focus(function () {
        $("#pnlErrorCotizador").hide();
    });

    $("#txtMunicipio").focus(function () {
        $("#pnlErrorCotizador").hide();
    });

    $("#ddlAnio").change(function () {
        $("#pnlErrorCotizador").hide();

        $('#txtMunicipio').val('');
        $('#txtModelo').val('');
        $("#municipioSeleccionado").val('');
        $("#entidadFederativa").val('');
        $("#descripcionMunicipio").val('');
        $("#descripcionEstado").val('');
        $("#modeloSeleccionado").val('');
    });

    //$("#IsAutoParticularCheckbox").change(function () {
     //   $("#pnlErrorCotizador").hide();
   // });

    $("#txtModelo").autocomplete({
        source: function (request, response) {
            $.getJSON(apiRoot + "Kwanko/BuscarModelos", {
                busqueda: request.term,
                anio: $("#ddlAnio").val()
            }, function (responseData) {
                response(responseData.items);
            });
        },
        position: { collision: 'flip' },
        minLength: 3,
        select: function (event, ui) {
            var val = ui.item.id;

            if (val !== "0") {
                $("#modeloSeleccionado").val(ui.item.id);
                console.log("Modelo seleccionado: " + ui.item.value + " aka " + ui.item.id);
                $('#txtMunicipio').focus();
            }

            if (val === "0") {
                $("#modeloSeleccionado").val('');
            }
        },
        open: function (){
            $(this).autocomplete('widget').css('z-index', 10000);
            return true;
        },
        focus: function (event, ui) {
            var val = ui.item.id;

            if (val !== "0") {
                $("#modeloSeleccionado").val(ui.item.id);
                console.log("Modelo seleccionado: " + ui.item.value + " aka " + ui.item.id);
                //$('#txtMunicipio').focus();
            }

            if (val === "0") {
                $("#modeloSeleccionado").val('');
            }

        }
    }).mousedown(function (e) {
        $(this).val('');
        $("#modeloSeleccionado").val('');
    });

    $("#txtMunicipio").autocomplete({
        position: { collision: 'flip' },
        source: function (request, response) {
            $.getJSON(apiRoot + "Kwanko/BuscarMunicipios", {
                busqueda: request.term
            }, function (responseData) {
                response(responseData.items);
            });
        },
        minLength: 3,
        select: function (event, ui) {
            $("#municipioSeleccionado").val(ui.item.id);
            $("#entidadFederativa").val(ui.item.EntidadFederativaId);            
            $("#descripcionMunicipio").val(ui.item.value);
            $("#descripcionEstado").val(ui.item.Estado);
            console.log("Municipio seleccionado: " + ui.item.value + " aka " + ui.item.id);
            $('#btnCotizar').focus();
        },
        open: function () {
            $(this).autocomplete('widget').css('z-index', 10000);
            return true;
        },
        focus: function (e, ui) {
            $("#municipioSeleccionado").val(ui.item.id);
            $("#entidadFederativa").val(ui.item.EntidadFederativaId);
            $("#descripcionMunicipio").val(ui.item.value);
            $("#descripcionEstado").val(ui.item.Estado);
            console.log("Municipio seleccionado: " + ui.item.value + " aka " + ui.item.id);
            //alert(JSON.stringify(ui));
        }
    }).mousedown(function (e) {
        $(this).val('');
        $("#entidadFederativa").val('');
        $("#descripcionMunicipio").val('');
        $("#descripcionEstado").val('');
    });

    $("#btnCotizar").click(function () {
        enviarDatosFormulario();
    });

    // Reset de valores del cotizador.
    $('#txtMunicipio').val('');
    $('#txtModelo').val('');
    $('#ddlAnio').val('0');
    $("#municipioSeleccionado").val('');
    $("#entidadFederativa").val('');
    $("#descripcionMunicipio").val('');
    $("#descripcionEstado").val('');
    $("#modeloSeleccionado").val('');
   // $('#IsAutoParticularCheckbox').removeAttr('checked');
};

$(function () {
    inicializarCotizador();
});