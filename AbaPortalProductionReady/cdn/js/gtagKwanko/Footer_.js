﻿document.getElementById("footertelefono").addEventListener("click", funfootertelefono);
document.getElementById("footeremail").addEventListener("click", funfooteremail);
document.getElementById("footerEstado").addEventListener("click", funfooterEstado);
document.getElementById("footerMunicipio").addEventListener("click", funfooterMunicipio);

function funfootertelefono() {
    gtag('event', '01800', { 'event_category': 'footer', 'event_label': '' });
}
function funfooteremail() {
    gtag('event', 'mail', { 'event_category': 'footer', 'event_label': '' });
}
function funfooterEstado() {
    gtag('event', 'localizador_de_oficinas', { 'event_category': 'footer', 'event_label': 'estado' });
}
function funfooterMunicipio() {
    gtag('event', 'localizador_de_oficinas', { 'event_category': 'footer', 'event_label': 'municipio' });
}