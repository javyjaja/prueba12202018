﻿document.getElementById("autoSeguro").addEventListener("click", funautoSeguro);
document.getElementById("autoIndex").addEventListener("click", funautoIndex);
document.getElementById("hogarSeguro").addEventListener("click", funhogarSeguro);
document.getElementById("hogarIndex").addEventListener("click", funhogarIndex);
document.getElementById("pymeSeguro").addEventListener("click", funpymeSeguro);
document.getElementById("otrosSeguros").addEventListener("click", funotrosSeguros);

function funautoSeguro() {
    gtag('event', 'auto_seguro', { 'event_category': 'nuestros_seguros', 'event_label': 'ver_mas' });
}
function funautoIndex() {
    gtag('event', 'auto_seguro', { 'event_category': 'nuestros_seguros', 'event_label': 'cotizar' });
}
function funhogarSeguro() {
    gtag('event', 'hogar_seguro', {'event_category': 'nuestros_seguros','event_label': 'ver_mas'});
}
function funhogarIndex() {
    gtag('event', 'hogar_seguro', { 'event_category': 'nuestros_seguros', 'event_label': 'cotizar' });
}
function funpymeSeguro() {
    gtag('event', 'pyme_segura', { 'event_category': 'nuestros_seguros', 'event_label': 'ver_mas' });
}
function funotrosSeguros() {
    gtag('event', 'otros_seguros', { 'event_category': 'nuestros_seguros', 'event_label': 'ver_mas' });
}
