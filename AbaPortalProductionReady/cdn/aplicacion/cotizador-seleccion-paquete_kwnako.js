﻿
var cotizacion = {};
var periodicidad = "mensual";
var habilitarImpresion = false;

var imprimirPaqueteSeleccionado = function (paqueteId) {
    var paqueteSeleccionado = {};

    $.each(cotizacion.Paquetes, function (index, paquete) {
        if (paquete.PaqueteId + "" === paqueteId + "") {
            var monto = 0;
            var primerPago = 0;
            var cantidadPagos = 0;
            var pagoSubsecuente = 0;

            var imprimirPrimerPago = true;

            switch (periodicidad) {
                case 'mensual':
                    monto = paquete.PrimaTotalMensualFormato;
                    primerPago = paquete.PrimerPagoMensual;
                    pagoSubsecuente = paquete.PagoSubsecuenteMensual;
                    cantidadPagos = 11;
                    break;
                case 'trimestral':
                    monto = paquete.PrimaTotalTrimestralFormato;
                    primerPago = paquete.PrimerPagoTrimestral;
                    pagoSubsecuente = paquete.PagoSubsecuenteTrimestral;
                    cantidadPagos = 3;
                    break;
                case 'semestral':
                    monto = paquete.PrimaTotalSemestralFormato;
                    primerPago = paquete.PrimerPagoSemestral;
                    pagoSubsecuente = paquete.PagoSubsecuenteSemestral;
                    cantidadPagos = 1;
                    break;
                case 'anual':
                    monto = paquete.PrimaTotalAnualFormato;
                    pagoSubsecuente = 0;
                    primerPago = paquete.PrimaTotalAnual;
                    cantidadPagos = 0;
                    break;
            }

            paqueteSeleccionado = {
                Vehiculo: cotizacion.Vehiculo,
                PaqueteId: paquete.PaqueteId,
                Periodicidad: periodicidad,
                Monto: monto,
                Descripcion: paquete.Descripcion,
                Vigencia: moment(paquete.InicioVigencia).format("L"),
                PrimerPago: '$' + (primerPago).formatMoney(2, '.', ','),
                ImprimirPrimerPago: imprimirPrimerPago,
                CantidadPagos: cantidadPagos,
                PagoSubsecuente: '$' + (pagoSubsecuente).formatMoney(2, '.', ',')
            };
            $("#leadGenMontoPaquete").val(monto);
            $("#leadGenPaquete").val(paquete.Descripcion);
            $("#leadGenFormaPago").val(periodicidad);

            $("#pnlSeleccionPaquete").load("/Kwanko/PaqueteSeleccionado_", { paquete: paqueteSeleccionado });
            $("#seleccionPaquete").val(paquete.PaqueteId);
            $("#seleccionPaqueteInformacion").val(paquete.Info);
            $("#vehiculo").val(cotizacion.Vehiculo);

            window.localStorage.setItem("kwankoPaqueteId", paquete.PaqueteId);
            window.localStorage.setItem("monto", primerPago);
            //console.log("paquete info:" + paquete.Info);
        } //endif
    });
    console.log("Paquete seleccionado Id:" + paqueteId);
};

var imprimirSelectorPaquetes = function (paquetes) {
    var paquetesConFormato = [];

    console.log("Periodicidad:" + periodicidad);
    for (var i=0; i<4; i++){
        if (i == 0) paquetes[i].videoSrc = "https://www.youtube.com/embed/IlIntNdIC-8?ecver=2";
        else if (i == 1) paquetes[i].videoSrc = "https://www.youtube.com/embed/rz4DxhqL7NA?ecver=2";
        else if (i == 2) paquetes[i].videoSrc = "https://www.youtube.com/embed/4VDn9lCiP_s?ecver=2";
        else paquetes[i].videoSrc = "https://www.youtube.com/embed/R2eMcs_g9cM?ecver=2";
    }
    var valido = true;
    $.each(paquetes, function (index, paquete) {
        var monto = 0;
        switch (periodicidad) {
            case 'mensual':
                monto = paquete.PrimaTotalMensualFormato;
                break;
            case 'trimestral':
                monto = paquete.PrimaTotalTrimestralFormato;
                break;
            case 'semestral':
                monto = paquete.PrimaTotalSemestralFormato;
                break;
            case 'anual':
                monto = paquete.PrimaTotalAnualFormato;
                break;
        }
        
        paquetesConFormato.push({
            PaqueteId: paquete.PaqueteId,
            PaquetePreferente: paquete.PaquetePreferente,
            Descripcion: paquete.Descripcion,
            Monto: monto,
            ClasePreferente: paquete.PaquetePreferente === true ? " opcion-recomendada" : "",
            VideoSrc: paquete.videoSrc
        });
    });
   
    $("#seleccionCoberturasStack").load("/Kwanko/ListadoPaquetes_", { paquetes: paquetesConFormato });
    imprimirPaqueteSeleccionado(cotizacion.PaqueteSeleccionadoId);
    
};

var initialize = function () {
    $.ajax({
        url: apiRoot + "Kwanko/ObtenerPaquetes",
        method: 'POST',
        data: $("#frmDatosCotizacion").serialize(),
        beforeSend: function() {
            $('#Lightbox').show();
        },
        complete: function() {
            $('#Lightbox').hide();
        },
        success: function (response) {
            var valido = true;
            var products = [];
            cotizacion = response.cotizacion;
            $.each(cotizacion.Paquetes, function (index, paquete) {
                if (paquete.PrimaTotalAnual == 0)
                    valido = false;
                products.push(paquete.PaqueteId);
            });
            if (valido) {
                window.ptag_params = {
                    zone: "listing",
                    categoryId: "",
                    products: products,
                    customerId: "",
                    siteType: "d",
                    m_md5: ""
                };
            imprimirSelectorPaquetes(cotizacion.Paquetes);
            imprimirPaqueteSeleccionado(cotizacion.PaqueteSeleccionadoId);
            $("#panelTablaComparativa").html(response.VistaTablaComparativa);
            $('#vehiculoCotizado').html(sessionStorage.getItem('autoCotizado'));
            habilitarImpresion = true;
            } else {
                $('#datosPaqueteContainer').html('');
                $("#mensajeErrorPaquetes").show();
            }
        }
    });
};

var modoImpresionPoliza = "impresion";

var inicializarComandosImpresionCotizacion = function () {
    function base64ToArrayBuffer(base64) {
        var binaryString = window.atob(base64);
        var binaryLen = binaryString.length;
        var bytes = new Uint8Array(binaryLen);
        for (var i = 0; i < binaryLen; i++) {
            var ascii = binaryString.charCodeAt(i);
            bytes[i] = ascii;
        }
        return bytes;
    }

    var validarFormularioImpresion = function () {
        var isValid = true;

        if (!($("#txtNombreCotizacion").val().length > 0)) {
            isValid = false;
            $("#containerNombreCotizacion").addClass("has-warning");
            $("#lblErrorNombre").show();
        }

        if (modoImpresionPoliza === "correo") {
            if (!($("#txtCotizacionCorreoElectronico").val().length > 0)) {
                isValid = false;
                $("#containerCorreoCotizacion").addClass("has-warning");
                $("#lblErrorCorreo").html('<small>Es necesario que indique su correo electr&oacute;nico</small>')
                $("#lblErrorCorreo").show();
            }
            else {
                if (!utils.IsValidEmailAddress($("#txtCotizacionCorreoElectronico").val())) {
                    $("#containerCorreoCotizacion").addClass("has-warning");
                    $("#lblErrorCorreo").html('<small>Direcci&oacute;n de correo inv&aacute;lida. Por favor verifique.</small>')
                    $("#lblErrorCorreo").show();
                    isValid = false;
                }

                if (utils.IsValidEmailAddress($("#txtCotizacionConfirmaCorreoElectronico").val())) {
                    var correo = $("#txtCotizacionCorreoElectronico").val();
                    var confirmacion = $("#txtCotizacionConfirmaCorreoElectronico").val();

                    if (correo !== confirmacion) {
                        $("#containerConfirmaCorreoCotizacion").addClass("has-warning");
                        $("#lblErrorConfirmarCorreo").html('<small>Valide su direcci&oacute;n de correo el&eacute;ctronico..</small>')
                        $("#lblErrorConfirmarCorreo").show();
                        isValid = false;
                    }
                }
                else
                {
                    $("#containerConfirmaCorreoCotizacion").addClass("has-warning");
                    $("#lblErrorConfirmarCorreo").html('<small>Direcci&oacute;n de correo inv&aacute;lida. Por favor verifique.</small>')
                    $("#lblErrorConfirmarCorreo").show();
                    isValid = false;
                }


            }
        }

        return isValid;
    }

    $("#txtNombreCotizacion").focus(function () {
        $(this).parent().removeClass("has-warning");
        $("#lblErrorNombre").hide();
    });

    $("#txtCotizacionCorreoElectronico").focus(function () {
        $(this).parent().removeClass("has-warning");
        $("#lblErrorCorreo").hide();
    });

    $("#txtCotizacionConfirmaCorreoElectronico").focus(function () {
        $(this).parent().removeClass("has-warning");
        $("#lblErrorConfirmarCorreo").hide();
    });

    $("#lnkImprimirCotizacion").click(function () {
        if (habilitarImpresion === true) {
            $("#btnImprimirCotizacion").removeAttr("disabled");
            modoImpresionPoliza = "impresion";
            $("#pnlImprimirCotizacionCorreoEl").hide();
            $('#pnlImprimirCotizacionModal').modal({
                keyboard: true
            });

            $('#lblNombre').html('A nombre de quién quieres imprimir tu cotización: ');

            $("#pnlImprimirCotizacionModal").find(".modal-title").html("Imprimir cotización");
            $("#btnImprimirCotizacion").html("Imprimir cotización");
        }
        else {
            //alert("Información de paquetes no disponible aún.  Por favor espere.")
        }
        return false;
    });

    $("#chkAceptarImpresion").change(function () {
        if ($('#chkAceptarImpresion').is(':checked')) {
            $("#btnImprimirCotizacion").removeAttr("disabled");
        }
        else {
            $("#btnImprimirCotizacion").attr("disabled", "disabled");
        }
    });

    $("#lnkImprimirCotizacionCorreo").click(function () {
        if (habilitarImpresion === true) {
            modoImpresionPoliza = "correo";
            $("#chkAceptarImpresion").removeAttr("checked");
            $("#btnImprimirCotizacion").attr("disabled", "disabled");
            $("#pnlImprimirCotizacionCorreoEl").show();
            $("#txtCotizacionCorreoElectronico").val("");
            $('#txtCotizacionConfirmaCorreoElectronico').val("");
            $('#pnlImprimirCotizacionModal').modal({
                keyboard: true
            });

            $('#lblNombre').html('A nombre de quién enviamos tu cotización: ');
            $("#pnlImprimirCotizacionModal").find(".modal-title").html("Enviar cotización");
            $("#btnImprimirCotizacion").html("Enviar cotización");
        }
        else {
            alert("Información de paquetes no disponible aún.  Porfavor espere.")
        }

        return false;
    });

    $("#btnImprimirCotizacion").click(function () {
        $("#nombreUsuario").val($("#txtNombreCotizacion").val());
        if (validarFormularioImpresion()) {
            var html = $(this).html();

            if (html === 'Enviar cotización') {
                $('#btnImprimirCotizacion').attr({ 'disabled': 'disabled' });
                $('#loadingEnviandoMail').show();
                $("#correoElectronicoUsuario").val($("#txtCotizacionCorreoElectronico").val());
                $.post(apiRoot + "Kwanko/EnviarCotizacion", $("#frmFormularioComprar").serialize(), function (res) {
                    $("#btnImprimirCotizacion").removeAttr("disabled");
                    //$('#pnlImprimirCotizacionModal').modal('hide');
                    $('#pnlImprimirCotizacionModal #pnlImprimirCotizacionCorreoEl').slideUp();
                    $('#pnlImprimirCotizacionModal #containerNombreCotizacion').slideUp();
                    if (res.Response) {
                        $('#pnlImprimirCotizacionModal #containerMessageSent').slideDown();
                    } else {
                        $('#pnlImprimirCotizacionModal #containerMessageError').slideDown();
                    }
                    $('#containerPrint').hide();
                    $('#btnImprimirCotizacion').hide();
                }).fail(function () {
                    $('#containerPrint').hide();
                })
                .always(function () {
                    $('#btnImprimirCotizacion').removeAttr('disabled');
                    $('#loadingEnviandoMail').hide();
                });
            }
            else {
                $.ajax({
                    url: apiRoot + "Kwanko/GenerarCotizacion",
                    method: 'POST',
                    data: $("#frmFormularioComprar").serialize(),
                    dataType: 'binary',
                    processData: false,
                    beforeSend: function (xhr, settings) {
                        $('#containerPrint').show();
                        $("#btnImprimirCotizacion").attr("disabled", "disabled");
                        $("#nombreUsuario").val($("#txtNombreCotizacion").val());
                        $("#correoElectronicoUsuario").val($("#txtCotizacionCorreoElectronico").val());
                        //$("#periodicidad").val(periodicidad);
                    },
                    complete: function (xhr, textStatus) {
                        $('#containerPrint').hide();
                    },
                    error: function (xhr, textStatus, data) {
                        console.error(textStatus);
                        $('#containerPrint').hide();
                    },
                    success: function (response, status, xhr) {
                        $("#btnImprimirCotizacion").removeAttr("disabled");
                        $('#pnlImprimirCotizacionModal').modal('hide');
                        var filename = "";
                        var disposition = xhr.getResponseHeader("Content-Disposition");

                        if (disposition && disposition.indexOf("attachment") !== -1) {
                            var filenameRegex = /filename[^;=\n]*=(([""]).*?\2|[^;\n]*)/;
                            var matches = filenameRegex.exec(disposition);
                            if (matches !== null && matches[1]) {
                                filename = matches[1].replace(/[""]/g, "");
                            }
                        }

                        var type = xhr.getResponseHeader("Content-Type");
                        var data = new Blob([response], { type: type });

                        if (typeof window.navigator.msSaveBlob !== 'undefined') {
                            window.navigator.msSaveBlob(data, filename);
                        }
                        else {
                            var URL = window.URL || window.webkitURL;
                            var downloadURL = URL.createObjectURL(data);

                            if (filename) {
                                var a = document.createElement('a');

                                if (typeof a.download == 'undefined') {
                                    window.location = downloadURL;
                                }
                                else {
                                    a.href = downloadURL;
                                    a.download = filename;
                                    document.body.appendChild(a);
                                    a.click();
                                }
                            }
                            else {
                                window.location = downloadURL;
                            }

                            setTimeout(function () {
                                URL.revokeObjectURL(downloadURL);
                            }, 100);
                        }

                        $('#containerPrint').hide();
                    }
                });
            }
        }
        return false;
    });
};

var teLlamamos = function () {
    var isValid = true;
    if (!($("#txtNombreContactar").val().length > 0)) {
        isValid = false;
        $("#containerNombreCotizacion").addClass("has-warning");
        $("#lblErrorNombreContactar").slideDown();
    }
    if (!($("#txtTelefonoContactar").val().length > 0)) {
        isValid = false;
        $("#txtTelefonoContactar").addClass("has-warning");
        $("#lblErrorTelefonoContactar").slideDown();
    } else if (!($("#txtTelefonoContactar").val().length == 10)) {

        isValid = false;
        $("#txtTelefonoContactar").addClass("has-warning");
        $("#lblErrorTelefonoContactar").slideDown();
    }

    if (!($("#txtCorreoElectronicoContactar").val().length > 0)) {
        isValid = false;
        $("#txtCorreoElectronicoContactar").addClass("has-warning");
        $("#lblErrorCorreoContactar").html('<small>Es necesario que indique su correo electr&oacute;nico</small>')
        $("#lblErrorCorreoContactar").show();
    }
    else {
        if (!utils.IsValidEmailAddress($("#txtCorreoElectronicoContactar").val())) {
            $("#txtCorreoElectronicoContactar").addClass("has-warning");
            $("#lblErrorCorreoContactar").html('<small>Direcci&oacute;n de correo inv&aacute;lida. Por favor verifique.</small>')
            $("#lblErrorCorreoContactar").show();
            isValid = false;
        }
    }
    if (($("#txtTelefonoFijo").val().length > 0)) {
        if (!($("#txtTelefonoFijo").val().length == 10)) {

            isValid = false;
            $("#txtTelefonoFijo").addClass("has-warning");
            $("#lblErrorTelefonoFijo").slideDown();
        }
    } else {
        $("#lblErrorTelefonoFijo").slideUp();
    }

    if (isValid) {
        var paquete = $("#leadGenPaquete").val();
        var formaPago = $("#leadGenFormaPago").val();
        var montoPaquete = $("#leadGenMontoPaquete").val();
        var perfilCliente = "Propietario";
        var producto = "AUTO";
        $.ajax({
            url: webSiteRoot + "ServiciosExpress/LlamadaLeadGen",
            method: 'POST',
            data: {
                NombreCompleto: $("#txtNombreContactar").val(),
                Email: $("#txtCorreoElectronicoContactar").val(),
                TelefonoCompletoCelular: $("#txtTelefonoContactar").val(),
                TelefonoCompletoCasa: $("#txtTelefonoFijo").val() == null ? "" : $("#txtTelefonoFijo").val(),
                Origen: "Kwanko",
                Producto: producto,
                PaqueteSeleccionado: paquete,
                FormaPago: formaPago,
                MontoPaquete: montoPaquete,
                PerfilCliente: perfilCliente,
                TipoMotivoLlamada: 222,
                Modelo: $('[name=Modelo]').val(),
                Marca: $("#vehiculoCotizado").text(),
                Vehiculo: $("#vehiculoCotizado").text(),
                Municipio: $('[name=Municipio]').val(),
                Estado: $('[name=Municipio]').val()
            },
            beforeSend: function () {
                $('#Lightbox').find('#spinnerLight').contents()[2].nodeValue = 'Espere un momento ...';
                $('#Lightbox').show();
                $('#lblLeadGenErrorProceso').hide();
            },
            complete: function () {
                $('#Lightbox').hide();
            },
            success: function (response) {
                if (response.Response) {
                    $('#modalLlamamos').modal('hide');
                } else {
                    $('#lblLeadGenErrorProceso').show();
                }
            }
        });
    }
};

$(function () {
    $("#periodicidadId").val("1"); //iniciar con periodicidad 'Mensual'

    $("#btnMostrarComparativas,#btnMostrarTodasComparativas").click(function () {
        $("#panelSelectorCotizador").hide();
        $("#panelComparativas").show();
        $("#selectorMensualidades").hide();
        
        location.href = '#encabezadoCotizador';
    });

    $("#btnVolverASelectorPlanes").click(function () {
        $("#panelComparativas").hide();
        $("#selectorMensualidades").show();
        $("#panelSelectorCotizador").show();
        location.href = '#encabezadoCotizador';
    });

    $("input[data-behavior=mustbechecked]").change(function () {
        $("#pnlErrorPago").hide();
    });

    $(document).on("click", "#btnComprar", function () {
        var isValid = true;
        $('#Lightbox').show();
        $('#Lightbox').find('#spinnerLight').contents()[2].nodeValue = 'Espere un momento ...';
        $("input[data-behavior=mustbechecked]").each(function () {
            if (!$(this).is(":checked")) {
                isValid = false;
            }
        });

        if (!isValid) {
            $("#pnlErrorPago").html("Es necesaria la aceptaci&oacute;n de las pol&iacute;ticas se&ntilde;aladas.");
            $("#pnlErrorPago").show();
            $('#Lightbox').hide();
        } else {
            $("#periodicidad").val(periodicidad);
            $("#frmFormularioComprar").submit();
        }
    });

    $("button[data-action=periodicidad]").click(function () {

        //Aplicar estilo al boton activo:
        $("button[data-action=periodicidad]").attr("class", "btn btn-bordered");
        $(this).attr("class", "btn btn-bordered-active");

        periodicidad = $(this).attr("data-periodicidad");
        $('#periodicidad').val($(this).attr('data-periodicidad'));
        $("#periodicidadId").val($(this).attr("data-periodicidad-id"));
        console.log("PeriodicidadId:" + $("#periodicidadId").val());

        imprimirSelectorPaquetes(cotizacion.Paquetes);
    });

    $(document).on("click", "a[data-action=seleccionarPaquete]", function () {
        $("a[data-action=seleccionarPaquete]").parent().removeClass('opcion-recomendada');
        $(this).parent().addClass('opcion-recomendada');

        goToByScroll("pnlSeleccionPaquete");

        var paqueteId = $(this).attr("data-paqueteid");

        if (paqueteId == 1) {
            $(this).parent().parent().find('.opcion-recomendada-header').removeClass('notselected')
        } else {
            $(this).parent().parent().find('.opcion-recomendada-header').addClass('notselected')
        }

        console.log("Paquete seleccionado:" + paqueteId);
        imprimirPaqueteSeleccionado(paqueteId);
        return false;
    });

    function goToByScroll(id) {
        // Remove "link" from the ID
        id = id.replace("link", "");
        // Scroll
        $('html,body').animate({
            scrollTop: $("#" + id).offset().top -110
        },
            'slow');
    }

    //$("a").click(function () {
    //    idToGo = $(this).attr("href").replace("#", "");
    //    goToByScroll(idToGo);
    //});

    initialize();
    inicializarComandosImpresionCotizacion();
    $('#periodicidad').val('mensual');

        $('#divContactar').click(function () {
            $('#modalLlamamos').find('.has-warning').removeClass("has-warning");
            $('#modalLlamamos').find('.alert-warning').slideUp();
            $('#modalLlamamos').modal('show');
        });
        $('#btnComprarPaqueteCasa').click(function () {
            $('#frmFormularioComprar').submit();
        });

        $('#dudasChat').click(function () {
            $("#lnkLanzarChat").click();
            $('#chatHeader').click();
        });

        $('#txtNombreCotizacion, #txtNombreContactar').keypress(function (e) {
            var regex = /^[a-zA-Z\s]+$/
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }
            else {
                e.preventDefault();
                return false;
            }
        });

        $('#txtTelefonoContactar, #txtTelefonoFijo').on('keydown', function (e) {
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || (e.keyCode >= 35 && e.keyCode <= 40)) {
                return;
            }
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });


        $('#btnEnviarDatos').click(function () {
            teLlamamos();
        });
});