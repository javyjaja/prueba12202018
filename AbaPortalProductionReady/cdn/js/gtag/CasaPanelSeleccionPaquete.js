﻿document.getElementById("dudasChat").addEventListener("click", fundudasChat);
document.getElementById("divContactar").addEventListener("click", fundivContactar);
document.getElementById("btnEnviarDatos").addEventListener("click", funbtnEnviarDatos);
document.getElementById("btnCancelar").addEventListener("click", funbtnCancelar);
document.getElementById("arefLlamanos").addEventListener("click", funarefLlamanos);
document.getElementById("arefEmail").addEventListener("click", funarefEmail);

function fundudasChat() {

   gtag('event', 'lead_gen', {'event_category': 'hogar_cotizador','event_label': 'chat'});
}
function fundivContactar() {

    gtag('event', 'lead_gen', {'event_category': 'hogar_cotizador','event_label': 'te_llamamos'});
}
function funbtnEnviarDatos() {

   gtag('event', 'lead_gen', {'event_category': 'hogar_cotizador','event_label': 'te_llamamos_enviar'});
}
function funbtnCancelar() {

   gtag('event', 'nosotros_te_llamamos', { 'event_category': 'cotizador_Casa', 'event_label': 'cancelar' });
}
function funarefLlamanos() {

    gtag('event', 'lead_gen', {'event_category': 'hogar_cotizador','event_label': '1800'});
}
function funarefEmail() {

   gtag('event', 'lead_gen', {'event_category': 'hogar_cotizador','event_label': 'email'});
}