﻿document.getElementById("lnkImprimirConfirmacion").addEventListener("click", funlnkImprimirConfirmacion);
document.getElementById("lnkEnviarConfirmacion").addEventListener("click", funlnkEnviarConfirmacion);
document.getElementById("lnkImprimirPoliza").addEventListener("click", funlnkImprimirPoliza);
document.getElementById("lnkImprimirCondicionesGenerales").addEventListener("click", funlnkImprimirCondicionesGenerales);
document.getElementById("btnEnviarPoliza").addEventListener("click", funbtnEnviarPoliza);
document.getElementById("confirmacionCompra").addEventListener("click", funconfirmacionCompra);


function funlnkImprimirConfirmacion() {
    gtag('event', 'confirmacion_de_compra', { 'event_category': 'pyme_cotizador', 'event_label': 'imprime_pago' });
}
function funlnkEnviarConfirmacion() {
    gtag('event', 'confirmacion_de_compra', { 'event_category': 'pyme_cotizador', 'event_label': 'envia_pago' });
}
function funlnkImprimirPoliza() {
    gtag('event', 'confirmacion_de_compra', { 'event_category': 'pyme_cotizador', 'event_label': 'descarga_poliza' });
}
function funlnkImprimirCondicionesGenerales() {
    gtag('event', 'confirmacion_de_compra', { 'event_category': 'pyme_cotizador', 'event_label': 'descarga_cg' });
}
function funbtnEnviarPoliza() {
    gtag('event', 'confirmacion_de_compra', { 'event_category': 'pyme_cotizador', 'event_label': 'enviar_poliza' });
}
function funconfirmacionCompra() {
    gtag('event', 'confirmacion_de_compra', { 'event_category': 'pyme_cotizador', 'event_label': 'banner_lateral' });
}
