﻿
this.Initialize = function () {
    switch ($("#varOrden").val().toUpperCase()) {
        case "SI": $("#lbltituloEncuesta").text('Encuesta del reporte y ajustador');
            $("#lblSubtituloEncuesta").text('¿Cómo calificarías?');
            break;
        case "NO": $("#lbltituloEncuesta").text('Encuesta del reporte y ajustador');
            $("#lblSubtituloEncuesta").text('¿Cómo calificarías?');
            break;
        case "VA": $("#lbltituloEncuesta").text('Encuesta valuación');
            $("#lblSubtituloEncuesta").text('¿Que le pareció la atención del valuador? con respecto a:');
            break;
        case "TL": $("#lbltituloEncuesta").text('Encuesta de Taller (reparación de tu auto)');
            $("#lblSubtituloEncuesta").text('¿Cómo calificarías?');
            break;
        default: break;
    }
}

this.ValidaEncuesta = function () {
    var allChecked = false;

    var checked = $("#divEncuestaServicio :radio:checked");
    var groups = [];
    $("#divEncuestaServicio :radio").each(function () {
        if (groups.indexOf(this.name) < 0) {
            groups.push(this.name);
        }
    });
    if (groups.length == checked.length) {
        allChecked = true;
    }
    else {
        var total = groups.length - checked.length;
        allChecked = false;
    }
    return allChecked;
};

$(function () {
    Initialize();

    $('#btnEnvioEncuesta').unbind('click').click(function (e) {
        var totalCheck = $("#divEncuestaServicio :radio:checked").length;
        var strRespuestas = "";
        var respuestas = new Array();
        for (var c = 0; c < totalCheck; c++) {
            //respuestas.push($("#divEncuestaServicio :radio:checked")[c].id);
            strRespuestas += $("#divEncuestaServicio :radio:checked")[c].id + "|";
        }

        $.ajax({
            type: "POST",
            url: webSiteRoot + "CustomerSurvey/EnviaEncuesta",
            data: JSON.stringify({
                respuestasList: strRespuestas
            }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            headers: {
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
            },
            success: function (data) {
                if (data.toUpperCase() == "TRUE") {
                    var msg = $("#MsgEncuestaFin").val();
                    $("#divEncuestaServicio").html('');
                    $("#divEncuestaServicio").html('<br /><br /><br /><br /> <label class="lblErrorMsg">' + msg + '</label><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />');
                }
                else {
                    var msg = $("#MsgError").val();
                    $("#divEncuestaServicio").html('');
                    $("#divEncuestaServicio").html('<br /><br /><br /><br /> <label class="lblErrorMsg">' + msg + '</label><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />');
                }
            },
            error: function () {
                var msg = $("#MsgError").val();
                $("#divEncuestaServicio").html('');
                $("#divEncuestaServicio").html('<br /><br /><br /><br /> <label class="lblErrorMsg">' + msg + '</label><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />');
            }
        });
        e.preventDefault();
    });

    $(".checkEncuesta").unbind('change').change(function () {
        if (ValidaEncuesta() == true) {
            $("#btnEnvioEncuesta").removeAttr('disabled');
            $('#btnEnvioEncuesta').removeClass('btnEncuestaInactivo');
            $('#btnEnvioEncuesta').addClass('btnEncuestaActivo');
        }
    });

    $(".imgCheck").unbind('click').click(function () {
        var check = $(this)[0].name;
        var checkId = $(this)[0].id;
        var imgSplit;
        var clasesId;
        $("#" + check).prop('checked', true);
        if (ValidaEncuesta() == true) {
            $("#btnEnvioEncuesta").removeAttr('disabled');
            $('#btnEnvioEncuesta').removeClass('btnEncuestaInactivo');
            $('#btnEnvioEncuesta').addClass('btnEncuestaActivo');
        }
        var name = check.split("-");
        var group = name[0];
        var elementos = document.getElementsByName(group);
        var imagen;
        var classId;
        var imgClass;
        var lbl;
        for (var e = 0; e < elementos.length; e++) {
            imagen = $("#" + elementos[e].id + "-img");
            imagen[0].className = '';
            lbl = elementos[e].id.split('-');
            $("#" + imagen[0].id).addClass("imgCheck");
            imgClass = "img" + lbl[1];
            if (checkId == elementos[e].id + "-img")
                $("#" + imagen[0].id).addClass(imgClass);
            else
                $("#" + imagen[0].id).addClass(imgClass+"-Gris");
        }
    });

});