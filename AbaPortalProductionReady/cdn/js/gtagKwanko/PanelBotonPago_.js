﻿document.getElementById("btnOpenModalPagar").addEventListener("click", funbtnOpenModalPagar);
document.getElementById("btnAnterior").addEventListener("click", funbtnAnterior);
document.getElementById("btnPagar").addEventListener("click", funbtnPagar);
document.getElementById("btnNo").addEventListener("click", funbtnNo);

function funbtnOpenModalPagar() {
    gtag('event', 'pagar', { 'event_category': 'paga_tu_seguro_Kwanco', 'event_label': '' });
}

function funbtnAnterior() {
    history.go(-1);
    gtag('event', 'regresar', { 'event_category': 'paga_tu_seguro_Kwanco', 'event_label': '' });
}


function funbtnPagar() {
    gtag('event', 'contratar_y_pagar', { 'event_category': 'Formulario_de_pago_Kwanco', 'event_label': 'si' });
}

function funbtnNo() {
    gtag('event', 'contratar_y_pagar', { 'event_category': 'Formulario_de_pago_Kwanco', 'event_label': 'no' });
}