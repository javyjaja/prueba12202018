﻿$('#consultaPoliza').click(function (e) {
e.preventDefault();
$(this).find('.fa-spin').show();
$('#lblErrorConsulta').slideUp();
$('#Lightbox').show();
$('#Lightbox').find('#spinnerLight').contents()[2].nodeValue = 'Espere un momento ...';
var siniestro = $('#siniestro').val();
var clave = $('#clave').val();
var inciso = $('#inciso').val();

if (siniestro === '') {
    $('#Lightbox').hide();
    $('#lblErrorNumSiniestro').show();
    $(this).find('.fa-spin').hide();
    return false;
}
else if (clave === '') {
    $('#Lightbox').hide();
    $('#lblErrorAsegurado').show();
    $(this).find('.fa-spin').hide();
    return false;
}
else if (inciso === '') {
    $('#Lightbox').hide();
    $('#lblErrorInciso').show();
    $(this).find('.fa-spin').hide();
    return false;
}
else {
    $("#frmFormularioConsulta").submit()
}
});


$('#siniestro').focus(function (e) {
    $('#lblErrorNumSiniestro').hide();
});

$('#clave').focus(function (e) {
    $('#lblErrorAsegurado').hide();
});

$('#inciso').focus(function (e) {
    $('#lblErrorInciso').hide();
});

var error = decodeURIComponent(utils.GetQueryStringParams('e')).split('+').join(' ');

if (error !== "undefined") {
    $('#lblErrores .alert-warning > small').html(error);
    $('#lblErrores').show();
}